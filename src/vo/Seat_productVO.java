package vo;

public class Seat_productVO {
	private int st_id;
	private int sch_no;
	public Seat_productVO(int st_id, int sch_no) {
		this.st_id = st_id;
		this.sch_no = sch_no;
	}
	public int getSt_id() {
		return st_id;
	}
	public void setSt_id(int st_id) {
		this.st_id = st_id;
	}
	public int getSch_no() {
		return sch_no;
	}
	public void setSch_no(int sch_no) {
		this.sch_no = sch_no;
	}
	@Override
	public String toString() {
		return "Seat_productVO [st_id=" + st_id + ", sch_no=" + sch_no + "]";
	}
	
	
}
