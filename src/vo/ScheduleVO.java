package vo;

public class ScheduleVO {
	private int sch_no;
	private String sch_start;
	private int mv_id;
	public ScheduleVO(int sch_no, String sch_start, int mv_id) {
		this.sch_no = sch_no;
		this.sch_start = sch_start;
		this.mv_id = mv_id;
	}
	public ScheduleVO(String schStart) {
		this.sch_start = schStart;
	}
	public int getSch_no() {
		return sch_no;
	}
	public void setSch_no(int sch_no) {
		this.sch_no = sch_no;
	}
	public String getSch_start() {
		return sch_start;
	}
	public void setSch_start(String sch_start) {
		this.sch_start = sch_start;
	}
	public int getMv_id() {
		return mv_id;
	}
	public void setMv_id(int mv_id) {
		this.mv_id = mv_id;
	}
	@Override
	public String toString() {
		return "ScheduleVO [sch_no=" + sch_no + ", sch_start=" + sch_start + ",  mv_id=" + mv_id + "]";
	}
	
	
}
