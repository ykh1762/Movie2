package vo;

public class TicketVO {
	private int buy_id;
	private int st_id;
	private int sch_no;
	public TicketVO(int buy_id, int st_id, int sch_no) {
		super();
		this.buy_id = buy_id;
		this.st_id = st_id;
		this.sch_no = sch_no;
		
	}
	public TicketVO(int st_id) {
		super();
		this.st_id = st_id;
	}
	public TicketVO(int st_id2, int sch_no2) {
		// TODO Auto-generated constructor stub
	}
	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public int getSt_id() {
		return st_id;
	}
	public void setSt_id(int st_id) {
		this.st_id = st_id;
	}
	public int getSch_no() {
		return sch_no;
	}
	public void setSch_no(int sch_no) {
		this.sch_no = sch_no;
	}
	@Override
	public String toString() {
		return "TicketVO [buy_id=" + buy_id + ", st_id=" + st_id + ", sch_no=" + sch_no + "]";
	}

	
}
