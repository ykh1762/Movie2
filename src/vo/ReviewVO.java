package vo;

import java.sql.Date;

public class ReviewVO {
	private int reviewId;
	private String reviewScore;
	private String reviewContent;
	private Date reviewDate;
	private String seatId;
	private String scheduleNo;
	private String memId;
	private String mvId;
//	private String reviewScore2;
//	private String reviewContent2;
	
	


	public ReviewVO(String reviewScore, String reviewContent) {
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
	}

	public ReviewVO(int reviewId, String reviewScore, String reviewContent, Date reviewDate, String memId) {
		this.reviewId = reviewId;
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
		this.reviewDate = reviewDate;
		this.memId = memId;
	}

	public ReviewVO(int reviewId, String reviewScore, String reviewContent, Date reviewDate, String seatId,
			String scheduleNo) {
		this.reviewId = reviewId;
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
		this.reviewDate = reviewDate;
		this.seatId = seatId;
		this.scheduleNo = scheduleNo;
	}

	public ReviewVO(String reviewScore, String reviewContent, int sch_no, int st_id) {
		// TODO Auto-generated constructor stub
		this.seatId = String.valueOf(st_id);
		this.scheduleNo = String.valueOf(sch_no);
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
	}

	public ReviewVO(int reviewId, String reviewScore, String reviewContent, Date reviewDate, String seatId,
			String scheduleNo, String memId) {
		this.reviewId = reviewId;
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
		this.reviewDate = reviewDate;
		this.seatId = seatId;
		this.scheduleNo = scheduleNo;
		this.memId = memId;
	}

	public ReviewVO(int reviewId, String reviewScore, String reviewContent, Date reviewDate, String seatId,
			String scheduleNo, String memId, String mvId) {
		this.reviewId = reviewId;
		this.reviewScore = reviewScore;
		this.reviewContent = reviewContent;
		this.reviewDate = reviewDate;
		this.seatId = seatId;
		this.scheduleNo = scheduleNo;
		this.memId = memId;
		this.setMvId(mvId);
	}

	public ReviewVO(String reviewScore2, String reviewContent2, String st_id, String sch_no, String mvId2, String mem_id) {
		this.reviewScore =  reviewScore2;
		this.reviewContent = reviewContent2;
		this.seatId = st_id;
		this.scheduleNo = sch_no;
		this.mvId = mvId2;
		this.memId = mem_id;
		
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public String getReviewScore() {
		return reviewScore;//
	}

	public void setReviewScore(String reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getReviewContent() {
		return reviewContent;
	}

	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getSeatId() {
		return seatId;
	}

	public void setSeatId(String seatId) {
		this.seatId = seatId;
	}

	public String getScheduleNo() {
		return scheduleNo;
	}

	public void setScheduleNo(String scheduleNo) {
		this.scheduleNo = scheduleNo;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "ReviewVO [reviewId=" + reviewId + ", reviewScore=" + reviewScore + ", reviewContent=" + reviewContent
				+ ", reviewDate=" + reviewDate + ", seatId=" + seatId + ", scheduleNo=" + scheduleNo + ", memId="
				+ memId + "]";
	}

	public String getMvId() {
		return mvId;
	}

	public void setMvId(String mvId) {
		this.mvId = mvId;
	}

}
