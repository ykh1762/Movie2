package vo;

public class MemberVO {
	private int mem_no;
	private String mem_pass;
	private String mem_id;
	private String mem_name;
	private String mem_hp;
	private String mem_addr;
	private String mem_mail;
	private String mem_flag;
	private int mem_cash;
	private int mem_mileage;
	public MemberVO(int mem_no, String mem_pass, String mem_id, String mem_name, String mem_hp, String mem_addr,
			String mem_mail, String mem_flag, int mem_cash, int mem_mileage) {
		this.mem_no = mem_no;
		this.mem_pass = mem_pass;
		this.mem_id = mem_id;
		this.mem_name = mem_name;
		this.mem_hp = mem_hp;
		this.mem_addr = mem_addr;
		this.mem_mail = mem_mail;
		this.mem_flag = mem_flag;
		this.mem_cash = mem_cash;
		this.mem_mileage = mem_mileage;
	}
	public MemberVO(String mem_id,String mem_pass,String mem_name,String mem_hp,String mem_addr,String mem_mail) {
		this.mem_id = mem_id;
		this.mem_pass = mem_pass;
		this.mem_name = mem_name;
		this.mem_hp = mem_hp;
		this.mem_addr = mem_addr;
		this.mem_mail = mem_mail;
		
	}
	
	
	public MemberVO(String mem_id, int mem_no) {
		this.mem_no = mem_no;
		this.mem_id = mem_id;
	}
	public MemberVO(int memNo, String memId, String memName, String memHp, String memAdd, String memMail,
			String memFlag, int memCash, int memMile) {
		this.mem_addr = memAdd;
		this.mem_cash = memCash;
		this.mem_flag = memFlag;
		this.mem_hp = memHp;
		this.mem_id = memId;
		this.mem_mail =memMail;
		this.mem_mileage = memMile;
		this.mem_no = memNo;
		this.mem_name =memName;
		
	}
	public int getMem_no() {
		return mem_no;
	}
	public void setMem_no(int mem_no) {
		this.mem_no = mem_no;
	}
	public String getMem_pass() {
		return mem_pass;
	}
	public void setMem_pass(String mem_pass) {
		this.mem_pass = mem_pass;
	}
	public String getMem_id() {
		return mem_id;
	}
	public void setMem_id(String mem_id) {
		this.mem_id = mem_id;
	}
	public String getMem_name() {
		return mem_name;
	}
	public void setMem_name(String mem_name) {
		this.mem_name = mem_name;
	}
	public String getMem_hp() {
		return mem_hp;
	}
	public void setMem_hp(String mem_hp) {
		this.mem_hp = mem_hp;
	}
	public String getMem_addr() {
		return mem_addr;
	}
	public void setMem_addr(String mem_addr) {
		this.mem_addr = mem_addr;
	}
	public String getMem_mail() {
		return mem_mail;
	}
	public void setMem_mail(String mem_mail) {
		this.mem_mail = mem_mail;
	}
	public String getMem_flag() {
		return mem_flag;
	}
	public void setMem_flag(String mem_flag) {
		this.mem_flag = mem_flag;
	}
	public int getMem_cash() {
		return mem_cash;
	}
	public void setMem_cash(int mem_cash) {
		this.mem_cash = mem_cash;
	}
	public int getMem_mileage() {
		return mem_mileage;
	}
	public void setMem_mileage(int mem_mileage) {
		this.mem_mileage = mem_mileage;
	}
	@Override
	public String toString() {
		return "MemberVO [mem_no=" + mem_no + ", mem_pass=" + mem_pass + ", mem_id=" + mem_id + ", mem_name=" + mem_name
				+ ", mem_hp=" + mem_hp + ", mem_addr=" + mem_addr + ", mem_mail=" + mem_mail + ", mem_flag=" + mem_flag
				+ ", mem_cash=" + mem_cash + ", mem_mileage=" + mem_mileage + "]";
	}
	
	
	
}
