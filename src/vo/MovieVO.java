package vo;

public class MovieVO {
	//컬럼 수정 시 -> DB, VO, DAO, View 수정
	private int mvId;
	private String mvName;
//	private Date mv_date;
	private String mvDate; //Date
	private String mvGenre;
	private String mvSynopsis;
	private String mvDirector;
	private String mvDistributor;
	private String mvRate;
	private int mvAudience;
	private int mvTime;
	public MovieVO() {
	}
	
	public MovieVO(int mvId, String mvName) {
		this.mvId = mvId;
		this.mvName = mvName;
	}

	public MovieVO(int mvId, String mvName, String mvDate, String mvGenre, String mvSynopsis, String mvDirector,
			String mvDistributor, String mvRate, int mvAudience, int mvTime) {
		this.mvId = mvId;
		this.mvName = mvName;
		this.mvDate = mvDate;
		this.mvGenre = mvGenre;
		this.mvSynopsis = mvSynopsis;
		this.mvDirector = mvDirector;
		this.mvDistributor = mvDistributor;
		this.mvRate = mvRate;
		this.mvAudience = mvAudience;
		this.mvTime = mvTime;
	}

	public MovieVO(String mvName) {
		this.mvName= mvName;
	}

	public int getMvId() {
		return mvId;
	}
	public void setMvId(int mvId) {
		this.mvId = mvId;
	}
	public String getMvName() {
		return mvName;
	}
	public void setMvName(String mvName) {
		this.mvName = mvName;
	}
	public String getMvDate() {
		return mvDate;
	}
	public void setMvDate(String mvDate) {
		this.mvDate = mvDate;
	}
	public String getMvGenre() {
		return mvGenre;
	}
	public void setMvGenre(String mvGenre) {
		this.mvGenre = mvGenre;
	}
	public String getMvSynopsis() {
		return mvSynopsis;
	}
	public void setMvSynopsis(String mvSynopsis) {
		this.mvSynopsis = mvSynopsis;
	}
	public String getMvDirector() {
		return mvDirector;
	}
	public void setMvDirector(String mvDirector) {
		this.mvDirector = mvDirector;
	}
	public String getMvDistributor() {
		return mvDistributor;
	}
	public void setMvDistributor(String mvDistributor) {
		this.mvDistributor = mvDistributor;
	}
	public String getMvRate() {
		return mvRate;
	}
	public void setMvRate(String mvRate) {
		this.mvRate = mvRate;
	}
	public int getMvAudience() {
		return mvAudience;
	}
	public void setMvAudience(int mvAudience) {
		this.mvAudience = mvAudience;
	}
	public int getMvTime() {
		return mvTime;
	}
	public void setMvTime(int mvTime) {
		this.mvTime = mvTime;
	}
	@Override
	public String toString() {
		String substr_mv_syn = "";
		if (mvSynopsis.length() > 50) {
			substr_mv_syn = mvSynopsis.substring(0, 50) + "...";
		} else {
			substr_mv_syn = mvSynopsis;
		}
		return String.format("%d/%s/%s/%s/%s/%s/%s/%s/%d/%d", mvId, mvName, mvDate, mvGenre, substr_mv_syn,
				mvDirector, mvDistributor, mvRate, mvAudience, mvTime);
	}
	
	
	
}
