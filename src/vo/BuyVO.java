package vo;
//
public class BuyVO {
	int buy_id;
	int mem_no;
	int buy_cost;
	String buy_date;
	public BuyVO(int buy_id, int mem_no, int buy_cost, String buy_date) {
		super();
		this.buy_id = buy_id;
		this.mem_no = mem_no;
		this.buy_cost = buy_cost;
		this.buy_date = buy_date;
	}
	public BuyVO(int cost) {
		this.buy_cost = cost;
	}
	public BuyVO(String buyDate) {
		this.buy_date = buyDate;
	}
	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public int getMem_no() {
		return mem_no;
	}
	public void setMem_no(int mem_no) {
		this.mem_no = mem_no;
	}
	public int getBuy_cost() {
		return buy_cost;
	}
	public void setBuy_cost(int buy_cost) {
		this.buy_cost = buy_cost;
	}
	public String getBuy_date() {
		return buy_date;
	}
	public void setBuy_date(String buy_date) {
		this.buy_date = buy_date;
	}
	@Override
	public String toString() {
		return "BuyVO [buy_id=" + buy_id + ", mem_no=" + mem_no + ", buy_cost=" + buy_cost + ", buy_date=" + buy_date
				+ "]";
	}
	
	
}
