package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.driver.OracleDriver;
import vo.BuyVO;
import vo.MovieVO;
import vo.ScheduleVO;
import vo.TicketVO;

public class BuyDAO {
	private static BuyDAO instance = new BuyDAO();
	private BuyDAO() {
		
	}//
	public static BuyDAO getInstance() {
		return instance;
	}

	public static BuyVO getBuy(int mem_no) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT");
		builder.append("    buy_id,");
		builder.append("    mem_no,");
		builder.append("    buy_cost,");
		builder.append("    buy_date ");
		builder.append("FROM");
		builder.append("    buy");
		builder.append(" WHERE mem_no =" +"'"+mem_no+"'" );
		BuyVO buyvo =null;
		String sql = builder.toString();
		ResultSet resultset = statement.executeQuery(sql);
		if(resultset.next()) {
			int id = resultset.getInt("buy_id");
			int no = resultset.getInt("mem_no");
			int cost = resultset.getInt("buy_cost");
			String date = resultset.getString("buy_date");
			 buyvo = new BuyVO(id, no, cost, date);
		}
			

		return buyvo ;
	}
	
	public static List<Object> getTicket(int mem_no) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT");
		builder.append("    e.mv_name,");
		builder.append("    a.buy_date,");
		builder.append("    LISTAGG(b.st_id,");
		builder.append("        '>, <') WITHIN GROUP(");
		builder.append("        ORDER BY");
		builder.append("            a.buy_date");
		builder.append("        ) st_id,");
		builder.append("    d.sch_start,");
		builder.append("    a.buy_cost");
//		builder.append("    b.buy_id");////
		builder.append(" FROM");
		builder.append("    buy a,");
		builder.append("    ticket b,");
		builder.append("    member c,");
		builder.append("    schedule d,");
		builder.append("    movie e");
		builder.append(" WHERE");
		builder.append("    c.mem_no = " + mem_no);
		builder.append("    AND   c.mem_no = a.mem_no");
		builder.append("    AND   a.buy_id = b.buy_id");
		builder.append("    AND   d.sch_no = b.sch_no");
		builder.append("    AND   e.mv_id = d.mv_id");
		builder.append(" GROUP BY ");
		builder.append("    e.mv_name,");
		builder.append("    a.buy_date,");
		builder.append("    d.sch_start,");
		builder.append("    a.buy_cost");
		builder.append(" ORDER BY");
		builder.append("    a.buy_date");
		
		
		String sql = builder.toString();
		ResultSet resultSet = statement.executeQuery(sql);
		List<Object> list = new ArrayList<>();
		while(resultSet.next()) {
			int cost = resultSet.getInt("buy_cost");
			String buyDate = resultSet.getString("buy_date");
			String seatId = resultSet.getString("st_id");
			String schStart = resultSet.getString("sch_start");
			String mvName = resultSet.getString("mv_name");
			
		list.add(new MovieVO(mvName));
		list.add(new ScheduleVO(schStart));
		list.add(seatId);
		list.add(new BuyVO(cost));
		list.add(new BuyVO(buyDate));
			
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return list;
		
		
		
		
		
	}
	
	public int insertBuy(BuyVO buyVO) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" INSERT INTO buy (");
		builder.append("     buy_id,");
		builder.append("     mem_no,");
		builder.append("     buy_cost,");
		builder.append("     buy_date");
		builder.append(" ) VALUES (");
		builder.append("     buy_seq.NEXTVAL,");
		builder.append("     " + buyVO.getMem_no() + ",");
		builder.append("     " + buyVO.getBuy_cost() + ",");
		builder.append("     SYSDATE");
		builder.append(" )");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
//		statement.setInt(1, vo.getMv_no);
		
		int insertInt = statement.executeUpdate(sql);
		
		statement.close();
		connection.close();
		
		return insertInt;
	}
	
	public int getMaxBuy_id() throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     MAX(buy_id)");
		builder.append(" FROM");
		builder.append("     buy");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		
		int maxBuy_id = 0;
		
		ResultSet resultset = statement.executeQuery(sql);
		if(resultset.next()) {
			maxBuy_id = resultset.getInt("MAX(buy_id)");
		}		
		
		resultset.close();
		statement.close();
		connection.close();
		
		return maxBuy_id;
	}

}
