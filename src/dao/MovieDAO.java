package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.driver.OracleDriver;
import util.Connect;
import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;

public class MovieDAO {//
	private static MovieDAO instance = new MovieDAO();

	private MovieDAO() {

	}

	public static MovieDAO getInstance() {
		return instance;
	}

	public List<MovieVO> getMovieList() throws Exception {
		// build path - configure - add external jars - ojdbc8.jar 추가
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4",
				"java");
		Statement statement = connection.createStatement();
		String sql = "";
		sql += " SELECT";
		sql += "     mv_id,";
		sql += "     mv_name,";
		sql += "     mv_date,";
		sql += "     mv_genre,";
		sql += "     mv_synopsis,";
		sql += "     mv_director,";
		sql += "     mv_distributor,";
		sql += "     mv_rate,";
		sql += "     mv_audience,";
		sql += "     mv_time";
		sql += " FROM";
		sql += "     movie";
		sql += " ORDER BY";
		sql += "     mv_audience desc";

		ResultSet resultSet = statement.executeQuery(sql);
		List<MovieVO> list = new ArrayList<>();
		while (resultSet.next()) {
			int mvId = resultSet.getInt("mv_id");
			String mvName = resultSet.getString("mv_name");
			Date mvDate = resultSet.getDate("mv_date");
			String mvGenre = resultSet.getString("mv_genre");
			String mvSynopsis = resultSet.getString("mv_synopsis");
			String mvDirector = resultSet.getString("mv_director");
			String mvDistributor = resultSet.getString("mv_distributor");
			String mvRate = resultSet.getString("mv_rate");
			int mvAudience = resultSet.getInt("mv_audience");
			int mvTime = resultSet.getInt("mv_time");

			String strMvDate = String.valueOf(mvDate);
			list.add(new MovieVO(mvId, mvName, strMvDate, mvGenre, mvSynopsis, mvDirector, mvDistributor, mvRate,
					mvAudience, mvTime));
		}
		resultSet.close();
		statement.close();
		connection.close();

		return list;
	}

	public int insertMovie(MovieVO movieVO) {
		return 0;
	}

	/**
	 * 작성자 : PARK 작성일 : 2022. 11. 13. Method 설명 : 상영스케줄의 영화번호로 영화이름 조회.
	 * 
	 * @throws Exception
	 */
	public Map<String, MovieVO> getMvListBySch(List<ScheduleVO> list) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4",
				"java");
		PreparedStatement statement = null;

		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     a.mv_id,");
		builder.append("     a.mv_name,");
		builder.append("     b.sch_start");
		builder.append(" FROM");
		builder.append("     movie a,");
		builder.append("     (");
		builder.append("         SELECT");
		builder.append("             sch_no,");
		builder.append("             sch_start,");
		builder.append("             mv_id");
		builder.append("         FROM");
		builder.append("             schedule");
		builder.append("         ORDER BY");
		builder.append("             sch_start");
		builder.append("     )     b");
		builder.append(" WHERE");
		builder.append("     a.mv_id = b.mv_id");
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
//		statement.setInt(1, vo.getMv_name);

		ResultSet resultSet = statement.executeQuery(sql);
		HashMap<String, MovieVO> mvMap = new HashMap<>();
		while (resultSet.next()) {
			int mvId = resultSet.getInt("mv_id");
			String mvName = resultSet.getString("mv_name");
			String schStart = resultSet.getString("sch_start");
			mvMap.put(schStart, new MovieVO(mvId, mvName));
		}
		resultSet.close();
		statement.close();
		connection.close();

		return mvMap;
	}

	/**
	 * 작성자 : PC-25 작성일 : 2022. 11. 15. Method 설명 : 영화 번호로 영화 조회.
	 * 
	 * @throws Exception
	 */
	public MovieVO getMvByMvId(int searchMvId) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;

		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     mv_id,");
		builder.append("     mv_id,");
		builder.append("     mv_name,");
		builder.append("     mv_date,");
		builder.append("     mv_genre,");
		builder.append("     mv_synopsis,");
		builder.append("     mv_director,");
		builder.append("     mv_distributor,");
		builder.append("     mv_rate,");
		builder.append("     mv_audience,");
		builder.append("     mv_time ");
		builder.append(" FROM");
		builder.append("     movie");
		builder.append(" WHERE");
		builder.append("     mv_id = ?");

		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		statement.setInt(1, searchMvId);

		ResultSet resultSet = statement.executeQuery();
		MovieVO vo = null;

		if (resultSet.next()) {
			String mvName = resultSet.getString("mv_name");
			int mvId = resultSet.getInt("mv_id");
			Date mvDate = resultSet.getDate("mv_date");
			String mvGenre = resultSet.getString("mv_genre");
			String mvSynopsis = resultSet.getString("mv_synopsis");
			String mvDirector = resultSet.getString("mv_director");
			String mvDistributor = resultSet.getString("mv_distributor");
			String mvRate = resultSet.getString("mv_rate");
			int mvAudience = resultSet.getInt("mv_audience");
			int mvTime = resultSet.getInt("mv_time");
			String strMvDate = String.valueOf(mvDate);
			vo = new MovieVO(mvId, mvName, strMvDate, mvGenre, mvSynopsis, mvDirector, mvDistributor, mvRate, mvAudience, mvTime);
		}
		resultSet.close();
		statement.close();
		connection.close();

		return vo;
	}

	public List<MovieVO> getMvIdListByMemId(String memId) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT e.mv_id");
		builder.append(" FROM SCHEDULE E, (SELECT c.SCH_NO");
		builder.append("                     FROM TICKET C, (SELECT A.BUY_ID");
		builder.append("                                     FROM BUY A, MEMBER B");
		builder.append("                                       WHERE MEM_ID = '" + memId + "'");
		builder.append("                                      AND A.MEM_NO = B.MEM_NO) D");
		builder.append("                     WHERE C.BUY_ID = D.BUY_ID");
		builder.append("                     GROUP BY SCH_NO) F");
		builder.append(" WHERE E.SCH_NO = F.SCH_NO");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		
		ResultSet resultSet = statement.executeQuery();
		List<MovieVO> list = new ArrayList<>();
		while (resultSet.next()) {
			int mv_id = resultSet.getInt("mv_id");
			list.add(new MovieVO(mv_id, null));
		}
		resultSet.close();
		
		//int insertInt = statement.executeUpdate(sql);
		statement.close();
		connection.close();
		
		return list;
	}

	public int updateMovie(String data, String data2, MovieVO mvVO) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("UPDATE movie");
		builder.append("    SET");
		builder.append(" " +data +" =" + "'" + data2 + "' " );
		builder.append(" WHERE");
		builder.append(" mv_id = " + "'"+mvVO.getMvId()+"'");
		String sql = builder.toString();
		int update = statement.executeUpdate(sql);
		statement.close();
		connection.close();
		return update;
	}

}
