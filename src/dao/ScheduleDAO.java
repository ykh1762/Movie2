package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.driver.OracleDriver;
import vo.MovieVO;
import vo.ScheduleVO;

public class ScheduleDAO {
	private static ScheduleDAO instance = new ScheduleDAO();

	private ScheduleDAO() {

	}

	public static ScheduleDAO getInstance() {
		return instance;
	}
	
	public List<ScheduleVO> getScheduleList() throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     sch_no,");
		builder.append("     sch_start,");
		builder.append("     mv_id");
		builder.append(" FROM"); //query 복사할 때 앞에 띄어쓰기 넣기.
		builder.append("     schedule");
		builder.append(" ORDER BY");
		builder.append("     sch_start");
		String sql = builder.toString();
		
		ResultSet resultSet = statement.executeQuery(sql);
		List<ScheduleVO> list = new ArrayList<>();
		while (resultSet.next()) {
			int sch_no = resultSet.getInt("sch_no");
			String sch_start = resultSet.getString("sch_start");
			int mv_id = resultSet.getInt("mv_id");
			list.add(new ScheduleVO(sch_no, sch_start, mv_id));
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return list;
	}

	public ScheduleVO getSchedule(String searchSch_start) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     sch_no,");
		builder.append("     sch_start,");
		builder.append("     mv_id");
		builder.append(" FROM");
		builder.append("     schedule");
		builder.append(" WHERE");
		builder.append("     sch_start = TO_DATE('" + searchSch_start + "','YYYY/MM/DD HH24:MI')");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
//		statement.setString(1, searchSch_start); //prepared 왜 안되지?
		
		ResultSet resultSet = statement.executeQuery();
		ScheduleVO vo = null; 
		
		if (resultSet.next()) {
			int sch_no = resultSet.getInt("sch_no");
			String sch_start = resultSet.getString("sch_start");
			int mv_id = resultSet.getInt("mv_id");
			vo = new ScheduleVO(sch_no, sch_start, mv_id);
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return vo;
	}
}
