package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.driver.OracleDriver;
import vo.MemberVO;
import vo.ReviewVO;
import vo.TicketVO;

public class ReviewDAO {//
	private static ReviewDAO instance = new ReviewDAO();
	private ReviewDAO() {
	}
	
	public static ReviewDAO getInstance() {
		return instance;
	}
	
	public List<ReviewVO> getRevListByMvId(int searchMvId) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder  = new StringBuilder();
		builder.append("SELECT");
		builder.append("    *");
		builder.append(" FROM");
		builder.append("    review");
		builder.append(" WHERE");
		builder.append("    mv_id = " + searchMvId);////
		builder.append(" ORDER BY");
		builder.append(" rev_date desc");
		String sql = builder.toString();
		ResultSet resultSet = statement.executeQuery(sql);
		List<ReviewVO> list = new ArrayList<>();
		while(resultSet.next()) {
			String memId = resultSet.getString("mem_id");
			int reviewId = resultSet.getInt("rev_id");
			String reviewScore = resultSet.getString("rev_score");
			String reviewContent = resultSet.getString("rev_content");
			Date reviewDate = resultSet.getDate("rev_date");
//			String seatId = resultSet.getString("st_id");
//			String scheduleNo = resultSet.getString("sch_no");
			list.add(new ReviewVO(reviewId, reviewScore, reviewContent, reviewDate, memId));
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return list;	
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//		StringBuilder builder = new StringBuilder();
//		builder.append(" SELECT");
//		builder.append("   a.mem_id ,");
//		builder.append("     d.rev_id,");
//		builder.append("     d.rev_score,");
//		builder.append("     d.rev_content,");
//		builder.append("     d.rev_date");
//		builder.append(" FROM");
//		builder.append("     member a,");
//		builder.append("     buy b,");
//		builder.append("     ticket c,");
//		builder.append("     review d,");
//		builder.append("     movie e,");
//		builder.append("     schedule f");
//		builder.append(" WHERE");
//		builder.append("     a.mem_no = b.mem_no");
//		builder.append("     AND   b.buy_id = c.buy_id");
//		builder.append("     AND   c.sch_no = d.sch_no");
//		builder.append("     AND   c.sch_no = f.sch_no");
//		builder.append("     AND   e.mv_id = f.mv_id");
//		builder.append("     AND   e.mv_id = ?");
//		builder.append(" ORDER BY");
//		builder.append("     d.rev_id");
//		String sql = builder.toString();
//		PreparedStatement statement = connection.prepareStatement(sql);
//		statement.setInt(1, searchMvId);
	
//	public List<ReviewVO> getRevListByMv_name(String mv_name) throws Exception {
//		DriverManager.registerDriver(new OracleDriver());
//		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
//		
//		StringBuilder builder = new StringBuilder();		
//		builder.append(" SELECT");
//		builder.append("     a.rev_id,");
//		builder.append("     a.rev_score,");
//		builder.append("     a.rev_content,");
//		builder.append("     a.rev_date,");
//		builder.append("     a.st_id,");
//		builder.append("     a.sch_no");
//		builder.append(" FROM");
//		builder.append("     review a,");
//		builder.append("     schedule b,");
//		builder.append("     movie c");
//		builder.append(" WHERE");
//		builder.append("     a.sch_no = b.sch_no");
//		builder.append("     AND   c.mv_id = b.mv_id");
//		builder.append("     AND   c.mv_name = '" + mv_name + "'");
//		builder.append(" ORDER BY");
//		builder.append("     a.rev_id");
//
//		String sql = builder.toString();
//		Statement statement = connection.createStatement();
////		Statement statement = connection.prepareStatement(sql);
////	statement.setString(1, mv_name);
	
		
		
	
	public ReviewVO getReview(int searchNo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		
		
		
		builder.append(" SELECT");
		builder.append("    rev_id,");
		builder.append("    rev_score,");
		builder.append("    rev_content,");
		builder.append("    rev_date,");
		builder.append("    st_id,");
		builder.append("    sch_no");
		builder.append(" FROM");
		builder.append("    review");
		builder.append(" WHERE");
		builder.append("    rev_id = " + searchNo);
		String sql = builder.toString();
		
		ResultSet resultSet = statement.executeQuery(sql);
		ReviewVO vo = null;
		if (resultSet.next()) {
			int reviewId = resultSet.getInt("rev_id");
			String reviewScore = resultSet.getString("rev_score");
			String reviewContent = resultSet.getString("rev_content");
			Date reviewDate = resultSet.getDate("rev_date");
			String seatId = resultSet.getString("st_id");
			String scheduleNo = resultSet.getString("sch_no");
			vo = new ReviewVO(reviewId, reviewScore, reviewContent, reviewDate, seatId, scheduleNo);
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return vo;
		
	}
	
	public int insertReview(ReviewVO vo) throws SQLException {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		
		StringBuilder builder = new StringBuilder();
//		builder.append(" INSERT INTO review (");
//		builder.append("        rev_id,");
//		builder.append("        rev_score,");
//		builder.append("        rev_content,");
//		builder.append("        rev_date,");
//		builder.append("        st_id,");
//		builder.append("        sch_no");
//		builder.append("    ) VALUES (");
//		builder.append("        review_seq.nextval,");
//		builder.append("        '" + Integer.parseInt(vo.getReviewScore())  + "',");
//		builder.append("        '" + vo.getReviewContent() + "',");
//		builder.append("        SYSDATE,");
//		builder.append("        '" + Integer.parseInt(vo.getSeatId()) + "',");
//		builder.append("        '" + Integer.parseInt(vo.getScheduleNo()) + "'");
//		builder.append("    )");
		
		builder.append(" INSERT INTO review (");
		builder.append("     rev_id,");
		builder.append("     rev_score,");
		builder.append("     rev_content,");
		builder.append("     rev_date,");
		builder.append("     st_id,");
		builder.append("     sch_no,");
		builder.append("     mv_id,");
		builder.append("     mem_id");
		builder.append(" ) VALUES (");
		builder.append("     review_seq.NEXTVAL,");
		builder.append("        '" + Integer.parseInt(vo.getReviewScore())  + "',");
		builder.append("        '" + vo.getReviewContent() + "',");
		builder.append("        SYSDATE,");
		builder.append("        '" + Integer.parseInt(vo.getSeatId()) + "',");
		builder.append("        '" + Integer.parseInt(vo.getScheduleNo()) + "',");
		builder.append("     '" + Integer.parseInt(vo.getMvId())  + "',");
		builder.append("     '" + vo.getMemId() + "'");
		builder.append("    )");
		
		String sql = builder.toString();
		int update = statement.executeUpdate(sql);
		
		statement.close();
		connection.close();
		return update;
	}
	
	public int updateReview(ReviewVO vo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		StringBuilder builder = new StringBuilder();
		builder.append(" UPDATE review");
		builder.append("    SET");
		builder.append("        rev_score =?,");
		builder.append("        rev_content =?,");
		builder.append("        rev_date = SYSDATE ");
		builder.append(" WHERE");
		builder.append("    rev_id =?");
		
		String sql = builder.toString();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, vo.getReviewScore());
		statement.setString(2, vo.getReviewContent());
		statement.setInt(3, vo.getReviewId());
		
		int executeUpdate = statement.executeUpdate();
		statement.close();
		connection.close();
		return executeUpdate;
	}
	public int deleteReview(int deleteReview) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		StringBuilder builder = new StringBuilder();
		builder.append("DELETE FROM review WHERE");
		builder.append("    rev_id = ?");
		
		String sql = builder.toString();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, deleteReview);
		
		int executeUpdate= statement.executeUpdate();
		statement.close();
		connection.close();
		return executeUpdate;
	}

	public TicketVO getTicListByMemNo(int mem_no) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     a.st_id,");
		builder.append("     a.sch_no");
		builder.append(" FROM");
		builder.append("     ticket a,");
		builder.append("     (");
		builder.append("         SELECT");
		builder.append("             buy_id,");
		builder.append("             mem_no,");
		builder.append("             buy_cost,");
		builder.append("             buy_date");
		builder.append("         FROM");
		builder.append("             buy");
		builder.append("         WHERE");
		builder.append("             mem_no = ?");
		builder.append("     ) b");
		builder.append(" WHERE");
		builder.append("     a.buy_id = b.buy_id");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		statement.setInt(1, mem_no);
		
		ResultSet resultSet = statement.executeQuery();
		TicketVO vo = null;
		while (resultSet.next()) {
			int st_id = resultSet.getInt("st_id");
			int sch_no = resultSet.getInt("sch_no");
			vo = new TicketVO(st_id, sch_no);
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return vo;

	}

	public List<MemberVO> getMem_nameListByMv_name(String mv_name) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     a.mem_id");
		builder.append(" FROM");
		builder.append("     member a,");
		builder.append("     buy b,");
		builder.append("     ticket c,");
		builder.append("     review d,");
		builder.append("     movie e,");
		builder.append("     schedule f");
		builder.append("  WHERE");
		builder.append("     a.mem_no = b.mem_no");
		builder.append("     AND   b.buy_id = c.buy_id");
		builder.append("     AND   c.sch_no = d.sch_no");
		builder.append("     AND c.sch_no = f.sch_no");
		builder.append("     AND e.mv_id = f.mv_id");
		builder.append("     and e.mv_name = ?");
//		builder.append("     and e.mv_name = '" + mv_name + "'");
		builder.append(" ORDER BY");
		builder.append("     d.rev_id");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		statement.setString(1, mv_name);
		//리뷰 - 회원아이디 목록 제대로 안나옴.
		
//		String sql = builder.toString();
//		PreparedStatement statement = connection.prepareStatement(sql);
//		statement.setInt(1, deleteReview);
//		
//		int executeUpdate= statement.executeUpdate();
		
		ResultSet resultSet = statement.executeQuery();
		List<MemberVO> list = new ArrayList<>();
		while (resultSet.next()) {
			String mem_id = resultSet.getString("mem_id");
			list.add(new MemberVO(mem_id, 1));
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return list;
	}
	
	
	
}
