package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleDriver;
import vo.ScheduleVO;
import vo.TicketVO;

public class TicketDAO {
	private static TicketDAO instance = new TicketDAO();
	private TicketDAO() {
		
	}
	public static TicketDAO getInstance() {
		return instance;
	}
	
	/**
	 * 작성자 : PC-25
	 * 작성일 : 2022. 11. 15.
	 * Method 설명 : 상영 번호로 티켓 목록을 조회.
	 */
	public ArrayList<TicketVO> getTicListBySch_no(int searchSch_no) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT");
		builder.append("     buy_id,");
		builder.append("     st_id,");
		builder.append("     sch_no");
		builder.append(" FROM");
		builder.append("     ticket");
		builder.append(" WHERE");
		builder.append("     sch_no = ?");
		builder.append(" ORDER BY");
		builder.append("     st_id");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		statement.setInt(1, searchSch_no);
		
		ResultSet resultSet = statement.executeQuery();
		ArrayList<TicketVO> list = new ArrayList<>();
		while (resultSet.next()) {
			int buy_id = resultSet.getInt("buy_id");
			int st_id = resultSet.getInt("st_id");
			int sch_no = resultSet.getInt("sch_no");
			list.add(new TicketVO(buy_id, st_id, sch_no));
		}
		resultSet.close();
		statement.close();
		connection.close();
		
		return list;
	}
	
	public int insertTicket(TicketVO ticVO) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		PreparedStatement statement = null;
		
		StringBuilder builder = new StringBuilder();
		builder.append(" INSERT INTO ticket (");
		builder.append("     buy_id,");
		builder.append("     st_id,");
		builder.append("     sch_no");
		builder.append(" ) VALUES (");
		builder.append("     " + ticVO.getBuy_id() + ",");
		builder.append("     " + ticVO.getSt_id() + ",");
		builder.append("     " + ticVO.getSch_no());
		builder.append(" )");
		
		String sql = builder.toString();
		statement = connection.prepareStatement(sql);
		int insertInt = statement.executeUpdate(sql);
		
		statement.close();
		connection.close();
		
		return insertInt;
	}

}
