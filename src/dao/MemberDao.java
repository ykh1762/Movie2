package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.driver.OracleDriver;
import vo.MemberVO;

public class MemberDao {
	private static MemberDao instance = new MemberDao();
	private MemberDao() {
		// TODO Auto-generated constructor stub
	}
	public static MemberDao getInstance() {
		return instance;
	}
	
	
	
	
	
	
	
	
	
		
	public int insertMember(MemberVO membervo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		
		builder.append("INSERT");
		builder.append("    INTO member (");
		builder.append("        mem_no,");
		builder.append("        mem_id,");
		builder.append("        mem_pass,");
		builder.append("        mem_name,");
		builder.append("        mem_hp,");
		builder.append("        mem_addr,");
		builder.append("        mem_mail");
		builder.append("    )");
		builder.append("VALUES ( member_seq.nextval, ");
		

		builder.append("'"+membervo.getMem_id()+"' , ");
		builder.append("'"+membervo.getMem_pass()+"', ");
		builder.append("'"+membervo.getMem_name()+"', ");
		builder.append("'"+membervo.getMem_hp()+"', ");
		builder.append("'"+membervo.getMem_addr()+"', ");
		builder.append("'"+membervo.getMem_mail()+"' ) ");
		
		String sql = builder.toString();
		
		int insert = statement.executeUpdate(sql);
		
		statement.close();
		connection.close();
		return insert;
	}
	
	public MemberVO getMember(String loginId) throws Exception {
		// TODO Auto-generated method stub
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT");
		builder.append("    mem_no,");
		builder.append("    mem_pass,");
		builder.append("    mem_id,");
		builder.append("    mem_name,");
		builder.append("    mem_hp,");
		builder.append("    mem_addr,");
		builder.append("    mem_mail,");
		builder.append("    mem_flag,");
		builder.append("    mem_cash,");
		builder.append("    mem_mileage");
		builder.append(" FROM");
		builder.append("    member");
		builder.append(" WHERE mem_id = '"+loginId+"'");
		
		String sql = builder.toString();
		ResultSet resultSet = statement.executeQuery(sql);
		MemberVO vo = null;	
		if(resultSet.next()) {
			int no = resultSet.getInt("mem_no");
			String pass = resultSet.getString("mem_pass");
			String id   = resultSet.getString("mem_id");
			String name = resultSet.getString("mem_name");
			String hp   = resultSet.getString("mem_hp");
			String addr = resultSet.getString("mem_addr");
			String mail = resultSet.getString("mem_mail");
			String flag = resultSet.getString("mem_flag");
			int cash = resultSet.getInt("mem_cash"); 
			int mileage = resultSet.getInt("mem_mileage");
			vo = new MemberVO(no, pass, id, name, hp, addr, mail, flag, cash, mileage);
		}
		
		resultSet.close();
		statement.close();
		connection.close();
		
		return vo;
	}

	public int updateCash(MemberVO vo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("update member set mem_cash = " +"'"+ vo.getMem_cash()+ "'");
		builder.append("where mem_id =" +"'"+ vo.getMem_id() +"'");
		String sql = builder.toString();
		int update = statement.executeUpdate(sql);
		
		
		statement.close();
		connection.close();
		return update;
		
	}
	public int updatePass(MemberVO vo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("update member set mem_pass = "+"'"+vo.getMem_pass()+"'");
		builder.append("where mem_id = " +"'"+vo.getMem_id()+"'");
		String sql = builder.toString();
		int updatePass = statement.executeUpdate(sql);
		statement.close();
		connection.close();
		return updatePass;
	}
	public List<MemberVO> getMemberList() throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT");
		builder.append("    mem_no,");
		builder.append("    mem_id,");
		builder.append("    mem_name,");
		builder.append("    mem_hp,");
		builder.append("    mem_addr,");
		builder.append("    mem_mail,");
		builder.append("    mem_flag,");
		builder.append("    mem_cash,");
		builder.append("    mem_mileage");
		builder.append(" FROM");
		builder.append("    member");
		
		String sql =builder.toString();
		ResultSet resultSet = statement.executeQuery(sql);
		List<MemberVO> list = new ArrayList<>();
		MemberVO vo = null;
		while(resultSet.next()) {
		int memNo = resultSet.getInt("mem_no");
		String memId = resultSet.getString("mem_id");
		String memName = resultSet.getString("mem_name");
		String memHp = resultSet.getString("mem_hp");
		String memAdd = resultSet.getString("mem_addr");
		String memMail = resultSet.getString("mem_mail");
		String memFlag = resultSet.getString("mem_flag");
		int memCash = resultSet.getInt("mem_cash");
		int memMile = resultSet.getInt("mem_mileage");
		
		vo = new MemberVO(memNo,memId,memName,memHp,memAdd,memMail,memFlag,memCash,memMile);
		list.add(vo);
		
		
		}
		return list;
		
	}
	
	public int updateMember(String data, String data2,MemberVO memvo) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("UPDATE member");
		builder.append("    SET");
		builder.append(" " +data +" =" + "'" + data2 + "' " );
		builder.append(" WHERE");
		builder.append(" mem_id = " + "'"+memvo.getMem_id()+"'");
		String sql = builder.toString();
		int update = statement.executeUpdate(sql);
		statement.close();
		connection.close();
		return update;
	}
	public int deleteMember(int deleteNum) throws Exception {
		DriverManager.registerDriver(new OracleDriver());
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
		Statement statement = connection.createStatement();
		StringBuilder builder = new StringBuilder();
		builder.append("DELETE FROM member WHERE mem_no = " +deleteNum);
		String sql = builder.toString();
		int delete = statement.executeUpdate(sql);
		statement.close();
		connection.close();
		return delete;
		
		
		
	}
	
	
	
	
}
