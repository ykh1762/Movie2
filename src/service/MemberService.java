package service;

import java.util.List;

import dao.MemberDao;
import vo.MemberVO;

public class MemberService {
	
	MemberDao memdao = MemberDao.getInstance();
	public int insertMember(MemberVO memverVO) throws Exception {
		// TODO Auto-generated method stub
		return memdao.insertMember(memverVO);
	}
	public MemberVO getMember(String loginId) throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMember(loginId);
	}
	
	public int updateCash(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return memdao.updateCash(vo);
	}
	public int updatePass(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return memdao.updatePass(vo);
	}
	public List<MemberVO> getMemberList() throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMemberList();
	}
	
	public int updateMember(String data, String data2,MemberVO memvo) throws Exception {
		// TODO Auto-generated method stub
		return memdao.updateMember(data,data2,memvo);
	}
	public int deleteMember(int deleteNum) throws Exception {
		// TODO Auto-generated method stub
		return memdao.deleteMember(deleteNum);
	}
}
