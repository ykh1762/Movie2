package service;

import java.util.List;

//
import vo.BuyVO;
import vo.MemberVO;

public class BuyController {
	BuyService service = BuyService.getInstance();

	public static BuyVO getBuy(int mem_no) throws Exception {
		// TODO Auto-generated method stub
		return BuyService.getBuy(mem_no);
	}

	public static List<Object> getTicket(int mem_no) throws Exception {
		// TODO Auto-generated method stub
		return BuyService.getTicket(mem_no);
	}

	public int insertBuy(BuyVO buyVO) throws Exception {
		return service.insertBuy(buyVO);
	}

	public int getMaxBuy_id() throws Exception {
		return service.getMaxBuy_id();
	}

}
