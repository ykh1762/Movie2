package service;

import java.util.List;

import vo.MemberVO;

public class MemberController {
	MemberService service = new MemberService();

	public int insertMember(MemberVO memverVO) throws Exception {
		return service.insertMember(memverVO);
	}

	public MemberVO getMember(String loginId) throws Exception {
		// TODO Auto-generated method stub
		return service.getMember(loginId);
	}

	public int updateCash(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return service.updateCash(vo);
	}

	public int updatePass(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return service.updatePass(vo);
	}

	public List<MemberVO> getMemberList() throws Exception {
		// TODO Auto-generated method stub
		return service.getMemberList();
	}

//
	public int updateMember(String data, String data2, MemberVO memvo) throws Exception {
		return service.updateMember(data, data2, memvo);
	}

	public int deleteMember(int deleteNum) throws Exception {
		// TODO Auto-generated method stub
		return service.deleteMember(deleteNum);
	}

}
