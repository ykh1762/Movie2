package service;

import java.util.ArrayList;

import vo.TicketVO;

public class TicketController {
	TicketService service = TicketService.getInstance();

	private static TicketController instance = new TicketController();

	private TicketController() {

	}

	public static TicketController getInstance() {
		return instance;
	}

	public ArrayList<TicketVO> getTicListBySch_no(int timeMenu) throws Exception {
		return service.getTicListBySch_no(timeMenu);
	}

	public int insertTicket(TicketVO ticVO) throws Exception {
		return service.insertTicket(ticVO);
	}

}
