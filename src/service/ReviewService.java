package service;

import java.util.List;

import dao.ReviewDAO;
import vo.ReviewVO;
import vo.TicketVO;

public class ReviewService {
	ReviewDAO dao = ReviewDAO.getInstance();

	private static ReviewService instance = new ReviewService();

	private ReviewService() {
	}

	public static ReviewService getInstance() {
		return instance;
	}

	public ReviewVO getReview(int searchNo) throws Exception {
		return dao.getReview(searchNo);
	}

	public List<ReviewVO> getRevListByMvId(int mvId) throws Exception {
		return dao.getRevListByMvId(mvId);
	}

	public int insertReview(ReviewVO vo) throws Exception {
		return dao.insertReview(vo);
	}

	public int updateReview(ReviewVO vo) throws Exception {
		return dao.updateReview(vo);
	}

	public int deleteReview(int deleteReview) throws Exception {
		return dao.deleteReview(deleteReview);
	}

	public TicketVO getTicListByMemNo(int mem_no) throws Exception {
		return dao.getTicListByMemNo(mem_no);
	}
}
