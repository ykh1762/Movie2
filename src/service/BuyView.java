package service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import vo.BuyVO;
import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;
import vo.TicketVO;

public class BuyView {////
	public void buy(MovieVO mvVO, ScheduleVO schVO, List<Integer> seatList, MemberVO memVO) throws Exception {
		BuyController buyController = new BuyController();
		MemberController memController = new MemberController();
		TicketController ticController = TicketController.getInstance();

		boolean running = true;
		MovieView view = new MovieView();
		DecimalFormat formatter = new DecimalFormat("###,###");
//		formatter.format(111);

		String seatStr = "";

		for (int i = 0; i < seatList.size(); i++) {
			String modifiedMenu = "";
			if (seatList.get(i) < 10) {
				modifiedMenu = "0" + seatList.get(i);
			} else {
				modifiedMenu = String.valueOf(seatList.get(i));
			}

			if (i == 0) {
				seatStr += "<" + modifiedMenu + ">";
			} else {
				seatStr += ", <" + modifiedMenu + ">";
			}
		}

		while (running) {
			System.out.println("\n-------------예매 내역-------------");
			System.out.println("\n영화 : " + mvVO.getMvName());
			//// 요일, 마치는 시간.
			System.out.println("일시 : " + schVO.getSch_start().substring(0, schVO.getSch_start().length() - 3));
			System.out.println("인원 : " + seatList.size() + "명");
			System.out.println("좌석 : " + seatStr); //// 여기 출력 할때는 정렬해서 표시.
			System.out.println("금액 : " + formatter.format(seatList.size() * 13000) + "원");
			System.out.println("보유 캐시 : " + formatter.format(memVO.getMem_cash()) + "원");
//			System.out.println("(보유 마일리지) : " + formatter.format(memVO.getMem_mileage()) + "P\n");
			System.out.println("\n-----------------------------------");
			System.out.println("1.결제하기 | 2.캐시 충전 | 0.취소");
			System.out.println("-----------------------------------");
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());
			switch (menu) {
			case 1:
				// 보유 캐시, 마일리지 표시 (마일리지 사용 기능?)
				if (memVO.getMem_cash() > seatList.size() * 13000) {
					// 결제 완료. insertBuy(), insertTicket(), updateCash(memVO)
					BuyVO buyVO = new BuyVO(0, memVO.getMem_no(), seatList.size() * 13000, null);

					int insertInt = buyController.insertBuy(buyVO);
					if (insertInt == 1) {
						// 구매 생성 성공.
						// insertTicket()
						int insertTicInt = 0; // 티켓 생성 여부 확인
						TicketVO ticVO = null;
						for (int j = 0; j < seatList.size(); j++) {
							// insertBuy에서 buy_id가 맞게 들어가야 함. MAX(BUY_ID) (+1 이 아니네)
							int maxBuy_id = buyController.getMaxBuy_id();

							ticVO = new TicketVO(maxBuy_id, seatList.get(j), schVO.getSch_no());
//							System.out.println("***maxBuy_id"+maxBuy_id);////
							int tempInt = ticController.insertTicket(ticVO);
							if (tempInt == 1) {
								insertTicInt++;
							}
						}

						if (insertTicInt == seatList.size()) {
							// 회원 캐시 수정
							int updateCash = memVO.getMem_cash() - (seatList.size() * 13000);
							memVO.setMem_cash(updateCash);
							int updateInt = memController.updateCash(memVO);

							if (updateInt == 1) {
								// 회원 캐시 수정 완료
//								System.out.println("캐시ㅣㅣㅣ");////
							}

							// 티켓 생성 성공
							System.out.println("\n-------------------------");
							System.out.println("예매가 완료되었습니다.");
							System.out.println("영화 페이지로 이동합니다.");
							System.out.println("-------------------------");
							System.out.print("아무 키나 입력하세요. > ");
							scanner.nextLine();

							running = false;
							break;
						}
					} else {

					}
				} else {
					System.out.println("보유 캐시가 부족합니다.");
					System.out.print("아무 키나 입력하세요. > ");
					scanner.nextLine();
				}
				break;
			case 2:
//				formatter.format(111);
				System.out.println("\n------------------------------------");
				System.out.println("보유 캐시 : " + formatter.format(memVO.getMem_cash()) + "원");
				System.out.println("------------------------------------");
				System.out.print("충전할 금액을 입력하세요. > ");
				
				int money = scanner.nextInt();
				memVO.setMem_cash(memVO.getMem_cash() + money);
				int updateMember = memController.updateCash(memVO);

				if (updateMember == 1) {
					System.out.println("캐시 충전 완료");
					while (true) {
						System.out.print("아무 키나 입력하세요. > ");
						scanner.nextLine();
						break;
					}

				} else {
					System.out.println("캐시 충전 실패");
					break;
				}				
				break;
			case 3:
//				view.movie();
				break;
			case 0:
				running = false;
				break;
			}
		}

	}
}
