package service;

import java.util.List;
import java.util.Map;

import dao.MemberDao;
import dao.MovieDAO;
import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;

public class MovieService {//
	MovieDAO dao = MovieDAO.getInstance();
	MemberDao memdao =MemberDao.getInstance();
	
	//Singleton
	private static MovieService instance = new MovieService();
	private MovieService() {
		
	}
	public static MovieService getInstance() {
		return instance;
		//
	}
	
	public List<MovieVO> getMovieList() throws Exception {
		// TODO Auto-generated method stub
		return dao.getMovieList();
	}
	public int insertMovie(MovieVO movieVO) {
		// TODO Auto-generated method stub
		return dao.insertMovie(movieVO);
	}
	public Map<String, MovieVO> getMvListBySch(List<ScheduleVO> list) throws Exception {
		return dao.getMvListBySch(list);
	}
	public MovieVO getMvByMvId(int mvId) throws Exception {
		return dao.getMvByMvId(mvId);
	}
	public List<MovieVO> getMvIdListByMemId(String mem_id) throws Exception {
		return dao.getMvIdListByMemId(mem_id);
	}
	public int updateMovie(String data, String data2, MovieVO mvVO) throws Exception {
		return dao.updateMovie(data, data2, mvVO);
	}
	
}
