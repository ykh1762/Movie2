package service;

import java.util.List;
import java.util.Map;

import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;

//컨트롤러는 싱글톤으로 안만들어도 되나?
public class MovieController {
	private static MovieController instance = new MovieController();
	private MovieController() {
		
	}
	public static MovieController getInstance() {
		return instance ;
	}
	
	MovieService service = MovieService.getInstance();
	
	public List<MovieVO> getMovieList() throws Exception {
		return service.getMovieList();
	}

	public int insertMovie(MovieVO movieVO) {
		return service.insertMovie(movieVO);
	}
	public Map<String, MovieVO> getMvListBySch(List<ScheduleVO> list) throws Exception {
		return service.getMvListBySch(list);
	}
	public MovieVO getMvByMvId(int mvId) throws Exception {
		return service.getMvByMvId(mvId);
	}
	public List<MovieVO> getMvIdListByMemId(String mem_id) throws Exception {
		return service.getMvIdListByMemId(mem_id);
	}
	public int updateMovie(String data, String data2, MovieVO mvVO) throws Exception {
		return service.updateMovie(data, data2, mvVO);
	}

	

}
