package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;

public class ScheduleView {//
	ScheduleController controller = ScheduleController.getInstance();
	List<Boolean> schList = new ArrayList<>();

	public void schedule(MemberVO memVO) throws Exception {
		boolean running = true;
		Scanner scanner = new Scanner(System.in);

		while (running) {
			running = false; //// 이래야 영화 페이지로?
			// 일단 getScheduleList();
			List<ScheduleVO> list = controller.getScheduleList();

			timeTable(list);

			System.out.println("---------------------------------------------------");
			System.out.println("1.19일 | 2.20일 | 3.21일 | 4.22일 | 5.23일 | 0.뒤로");
			System.out.println("---------------------------------------------------");
			System.out.print("날짜 선택> ");

			int menu = Integer.parseInt(scanner.nextLine());
			int chooseDay = menu + 18;
			switch (menu) {
			case 1:
				chooseTime(list, chooseDay, memVO);
				break;
			case 2:
				chooseTime(list, chooseDay, memVO);
				break;
			case 3:
				chooseTime(list, chooseDay, memVO);
				break;
			case 4:
				chooseTime(list, chooseDay, memVO);
				break;
			case 5:
				chooseTime(list, chooseDay, memVO);
				break;
			case 0:
				running = false;
				break;
			}
		}
	}

	/**
	 * 작성자 : PARK 작성일 : 2022. 11. 14. Method 설명 : 상영 시간표 출력.
	 */
	private void timeTable(List<ScheduleVO> list) throws Exception {
		// mv_id로 mv_name 가져오기
		MovieController mvController = MovieController.getInstance();
		Map<String, MovieVO> mvMap = mvController.getMvListBySch(list); // mv_id, mv_name, sch_start

		//// 상영시간표가 제대로 나오고 있나? 22일 19:00에 상영 추가. 맞는거 같은데?
		System.out.println("\n\n---------------------------------------------------");
		System.out.println("11월 상영 시간표");
		System.out.println("---------------------------------------------------");
		System.out.println("       [19일]\t\t    [20일]     \t\t    [21일]     \t\t    [22일]     \t\t    [23일]");

		ArrayList<String> strList = new ArrayList<>();
		for (int i = 0; i < 25; i++) {
			boolean schFlag = false;
			String checkTime = (10 + (i / 5) * 3) + ":00";
			String checkDay = 19 + (i % 5) + "";

			// 10:00 19 / 10:00 20
			for (int j = 0; j < list.size(); j++) {
				String strTime = list.get(j).getSch_start().substring(11, 16);
				String strDay = list.get(j).getSch_start().substring(8, 10);

				String key = "2022-11-" + checkDay + " " + checkTime + ":00";

				if (checkTime.equals(strTime) && checkDay.equals(strDay)) {
					strList.add(strTime + " " + mvMap.get(key).getMvName());
					schFlag = true;
					break;
				}
			}

			if (schFlag) {
				// 상영 영화 있는지 여부
				schList.add(true);
				continue;
			} else {
				//// 상영 영화 없음 제대로 출력 안됨!
				strList.add(checkTime + " 상영 영화 없음");
				schList.add(!true);
			}
			// 10 13 16 19 22 //19 20 21 22 23일
			// 2022/11/13 10:00:00
		}

		ArrayList<String> modifiedStrList = new ArrayList<>();
		int size = 14;

		for (int i = 0; i < strList.size(); i++) {

			if (strList.get(i).length() > size) {
				// 정렬 해보려다가 패스함.
				modifiedStrList.add(convert(strList.get(i).substring(0, size) + "..", size + 10).trim());
//				modifiedStrList.add(convert(String.format("%-" + size + "s", strList.get(i).substring(0, size) + "..".trim()), size+10));
//				modifiedStrList.add(String.format("%-" + (size+5) + "s", convert(strList.get(i).substring(0, size) + "..", size+10).trim()));

			} else {
				String attachStr = "";
				for (int j = 0; j < size - strList.get(i).length(); j++) {
					attachStr += "  ";
				}
				modifiedStrList.add(strList.get(i) + attachStr);
//				modifiedStrList.add(convert(strList.get(i), size+10));
//				modifiedStrList.add(convert(String.format("%-" + size + "s", strList.get(i).trim()), size+10));
//				modifiedStrList.add(String.format("%-" + (size+5) + "s", convert(strList.get(i), size+10).trim()));
			}
		}

//		if (strList.get(i).substring(6).equals("상영 영화 없음")) {
		for (int i = 0; i < modifiedStrList.size(); i++) {
			if ((i + 1) % 5 == 0 && i != 0) {
				System.out.print(modifiedStrList.get(i) + "\n");
			} else {
				System.out.print(modifiedStrList.get(i) + "\t");
			}
		}
		System.out.println();
	}

	////
	public static String convert(String word, int size) {
		String formatter = String.format("%%%ds", size - getKorCnt(word));
		return String.format(formatter, word);
	}

	////
	private static int getKorCnt(String kor) {
		int cnt = 0;
		for (int i = 0; i < kor.length(); i++) {
			if (kor.charAt(i) >= '가' && kor.charAt(i) <= '힣') {
				cnt++;
			}
		}
		return cnt;
	}

	private void chooseTime(List<ScheduleVO> list, int chooseDay, MemberVO memVO) throws Exception {
		boolean timeRunning = true;
		Scanner scanner = new Scanner(System.in);

		//// 시간 선택할 때는 그 날짜의 영화만 출력. ..없이 넓게 출력.
		timeTable(list);
		System.out.println("--------------------------------------------------------");
		System.out.println("[" + chooseDay + "일] - 상영 시간을 선택해 주세요.");
		System.out.println("1.10:00 | 2.13:00 | 3.16:00 | 4.19:00 | 5.22:00 | 0.뒤로");
		System.out.println("--------------------------------------------------------");
		System.out.print("선택> ");
		int timeMenu = Integer.parseInt(scanner.nextLine());

		// chooseDay -> 19, 20, ...
		// 13:00, 21 -> (2, 3)
		//// index가 왜 -5가 나오지. 내일
		int index = ((timeMenu - 1) * 5 + (chooseDay - 18)) - 1;

//		System.out.println(schList.size() + " " + index);////
		//0 선택시 아래로 지나가도록 설정.
		if (timeMenu != 0) {
			if (!schList.get(index)) {
				timeRunning = false;
				System.out.println("--------------------------------------");
				System.out.println("해당 시간에 상영 중인 영화가 없습니다.");
				System.out.println("영화 페이지로 이동합니다.");
				System.out.println("--------------------------------------");
				System.out.print("아무 키나 입력하세요.> ");
				scanner.nextLine();
			}
		} 

		int chooseTime = 7 + 3 * timeMenu;
		String DayAndTime = String.valueOf(chooseDay + "/" + chooseTime);

		while (timeRunning) {
			timeRunning = false;
			switch (timeMenu) {
			case 1:
				chooseNumber(DayAndTime, memVO);
				break;
			case 2:
				chooseNumber(DayAndTime, memVO);
				break;
			case 3:
				chooseNumber(DayAndTime, memVO);
				break;
			case 4:
				chooseNumber(DayAndTime, memVO);
				break;
			case 5:
				chooseNumber(DayAndTime, memVO);
				break;
			case 0:
//				timeRunning = false;
				break;
			}
		}
	}

	private void chooseNumber(String DayAndTime, MemberVO memVO) throws Exception {
		boolean timeRunning = true;
		Scanner scanner = new Scanner(System.in);
		SeatView seatView = new SeatView();

		System.out.println("----------------------------------------------");
//		System.out.println("[19일], 13:00 - 인원 수를 선택해 주세요.");
		System.out.println("[" + DayAndTime.split("/")[0] + "일], " + DayAndTime.split("/")[1] + ":00 - 인원 수를 선택해 주세요.");
		System.out.println("1.1명 | 2.2명 | 3.3명 | 4.4명 | 5.5명 | 0.뒤로");
		System.out.println("----------------------------------------------");
		System.out.print("선택> ");
		int numberMenu = Integer.parseInt(scanner.nextLine());
		String strMenu = DayAndTime + "/" + String.valueOf(numberMenu); // day / time / number

		while (timeRunning) {
			timeRunning = false;
			switch (numberMenu) {
			case 1:
				seatView.seat(strMenu, memVO);
				break;
			case 2:
				seatView.seat(strMenu, memVO);
				break;
			case 3:
				seatView.seat(strMenu, memVO);
				break;
			case 4:
				seatView.seat(strMenu, memVO);
				break;
			case 5:
				seatView.seat(strMenu, memVO);
				break;
			case 0:
//				timeRunning = false;
				break;
			}
		}
	}

}
