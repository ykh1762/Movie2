package service;

import java.util.List;

import dao.BuyDAO;
import vo.BuyVO;
import vo.MemberVO;

public class BuyService {
	private static BuyService instance = new BuyService();

	private BuyService() {

	}

	public static BuyService getInstance() {
		return instance;
	}

	BuyDAO dao = BuyDAO.getInstance();

	public static BuyVO getBuy(int mem_no) throws Exception {
		// TODO Auto-generated method stub
		return BuyDAO.getBuy(mem_no);
	}

	public static List<Object> getTicket(int mem_no) throws Exception {
		// TODO Auto-generated method stub
		return BuyDAO.getTicket(mem_no);
	}

	public int insertBuy(BuyVO buyVO) throws Exception {
		return dao.insertBuy(buyVO);
	}

	public int getMaxBuy_id() throws Exception {
		return dao.getMaxBuy_id();
	}

}
