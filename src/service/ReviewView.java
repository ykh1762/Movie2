package service;

import java.util.List;
import java.util.Scanner;

import vo.MemberVO;
import vo.MovieVO;
import vo.ReviewVO;
import vo.TicketVO;

public class ReviewView {
	public void review(MemberVO vo, int mvId) throws Exception {
		boolean running = true;
		ReviewController controller = new ReviewController();
		MovieController mvController = MovieController.getInstance();

		while (running) {
			System.out.println("\n--------------------------------------------------------------");
			System.out.println("1.리뷰 목록 | 2.리뷰 등록 | 3.리뷰 수정 | 4.리뷰 삭제 | 0.뒤로");
			System.out.println("--------------------------------------------------------------");
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());

			switch (menu) { //
			case 1:
				System.out.printf("┌%50s┐\n",
						"─────────────────────────────────────────────────────────────────────────────────────────────────");
				System.out.printf("%5s %9s %30s %29s\n", "아이디", "별점", "리뷰내용", "작성일");
				System.out.printf("└%50s┘\n",
						"─────────────────────────────────────────────────────────────────────────────────────────────────");


				List<ReviewVO> list = controller.getRevListByMvId(mvId);
				
				for (ReviewVO review : list) {

					String substrRevCon;
					if (review.getReviewContent().length() > 35) {
						substrRevCon = review.getReviewContent().substring(0, 35) + "...";
					} else {
						substrRevCon = review.getReviewContent();
					}
					String[] scores = {"☆☆☆☆☆", "★☆☆☆☆", "★★☆☆☆", "★★★☆☆", "★★★★☆", "★★★★★"};

					String star = scores[Integer.parseInt(review.getReviewScore())];

					
					int a = Integer.parseInt(review.getReviewScore());

					
					if (substrRevCon.length() > 22) {
						System.out.print("  " + review.getMemId() + "\t" + star + "\t" + substrRevCon + "\t"
								+ review.getReviewDate() + "\n");
						// asd33** ★★★☆☆ 최고의 영화입니다. 2022-10-10
					} else {
						System.out.print("  " + review.getMemId() + "\t" + star + "\t" + substrRevCon + "\t\t\t\t"
								+ review.getReviewDate() + "\n");
					}
				}
				break;

			case 2:
				//회원 - 본 영화목록 조회 후 현재 영화번호와 일치하지 않으면 리뷰 등록불가.
				List<MovieVO> memMvList = mvController.getMvIdListByMemId(vo.getMem_id());
//				System.out.println(memMvList.size()+"////"+vo.getMem_id());////
				
//				System.out.println(mvId+"////");////
				boolean revFlag = false; //리뷰 가능 확인.
				for (int i = 0; i < memMvList.size(); i++) {
					if (mvId == memMvList.get(i).getMvId()) {
						revFlag = true;
					}else {
						//관람 X false
					}
				}
				
				if (revFlag) {
					//영화 관람O
					// memberVO - ticketVO - st_id, sch_no를 reviewVO에 입력 ////수정 필요.
					TicketVO ticVO = controller.getTicListByMemNo(vo.getMem_no());
					System.out.print("별점> ");
					String reviewScore = scanner.nextLine();
					System.out.print("내용> ");
					//System.out.println(vo.getMem_id());
					String reviewContent = scanner.nextLine();
					int insertReview = controller.insertReview(
						//	new ReviewVO(reviewScore, reviewContent, ticVO.getSch_no(), ticVO.getSt_id(), vo.getMem_id()));
							new ReviewVO(String.valueOf(reviewScore) , String.valueOf(reviewContent), String.valueOf(ticVO.getSt_id()), 
									String.valueOf(ticVO.getSch_no()),String.valueOf(mvId), vo.getMem_id() ));
					if (insertReview == 1) {
						System.out.println("리뷰 등록 완료!");
					} else {
						System.out.println("리뷰 등록 실패!");
					} 
				} else {
					System.out.println("관람한 영화에 대한 리뷰만 등록할 수 있습니다.");
				}
				
				break;

			case 3:
				
				System.out.print("수정할 리뷰 번호 입력> ");
				int searchNo = Integer.parseInt(scanner.nextLine());
				ReviewVO review = controller.getReview(searchNo);

				System.out.print("별점> ");
				String updateReviewScore = scanner.nextLine();
				if (updateReviewScore.length() > 0) {
					review.setReviewScore(updateReviewScore);
				}
				System.out.print("내용> ");
				String updateReviewContent = scanner.nextLine();
				if (updateReviewContent.length() > 0) {
					review.setReviewContent(updateReviewContent);
				}
				int updateReview = controller.updateReview(review);
				if (updateReview > 0) {
					System.out.println("리뷰 수정 성공!");
				} else {
					System.out.println("리뷰 수정 실패!");
				}
				break;

			case 4:
				System.out.println("삭제할 리뷰 번호 입력> ");
				int deleteNo = scanner.nextInt();
				ReviewVO deleteReview = controller.getReview(deleteNo);
				System.out.println(deleteReview);
				System.out.print("삭제하시겠습니까? 1. 삭제 2. 취소> ");
				int deleteStr = scanner.nextInt();
				if (deleteStr == 1) {
					if (controller.deleteReview(deleteNo) == 1) {
						System.out.println("삭제 성공!");
					} else {
						System.out.println("삭제 실패!");
					}
				}
				break;
			case 0:
				running = false;
			}
		}
	}

	
}
