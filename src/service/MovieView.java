package service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import vo.BuyVO;
import vo.MemberVO;
import vo.MovieVO;
import vo.ReviewVO;
import vo.ScheduleVO;
import vo.TicketVO;

public class MovieView {
	public void movie(MemberVO vo) throws Exception {
		if (!vo.getMem_flag().equals("Y")) {
			movieMem(vo);
		} else {
			// 관리자
			movieAdmin(vo);
		}
	}

	ReviewView view = new ReviewView();

//	MovieView view = new MovieView();
	private void movieAdmin(MemberVO vo) throws Exception {
		MemberController memController = new MemberController();

		boolean running = true;
		while (running) {
			MovieController controller = MovieController.getInstance();

			List<MovieVO> list = controller.getMovieList(); // 영화 목록 관객수 순으로 가져오도록 수정.

			System.out.println("\n---------------------------------------------------------------------------------");
			System.out.println("\t\t\t현재 상영중인 영화(누적 관객수 순)"); // 등록된 영화 = 상영중인 영화로 변경.
			System.out.println("---------------------------------------------------------------------------------");
//			for (MovieVO movie : list) {
//				System.out.println(movie + "\n");
//			}

			// 관객 수 콤마 찍기
			DecimalFormat formatter = new DecimalFormat("###,###");
//			formatter.format(111);

			// 가로 막대
			for (int i = 0; i < list.size(); i++) {
				for (int j = 0; j < 130; j++) {
					if (j == 0) {
//						System.out.print("   " + date[i] + "   ");
						System.out.print("  ");
					} else {
						System.out.print("■");
						if ((j + 1) * 160000 >= list.get(i).getMvAudience()) {
							System.out.print(" " + (i + 1) + "." + list.get(i).getMvName() + " "
									+ formatter.format(list.get(i).getMvAudience()) + "명");
							break;
						}
					}
				}
//				System.out.println(" " + list.get(i).getMvAudience());
				System.out.println();
//				System.out.println((i+1) + ". " + list.get(i).getMvName() + " | " + list.get(i).getMvGenre() + " | " + list.get(i).getMvRate() + " | " + list.get(i).getMvDirector() + " | " + list.get(i).getMvTime() + " | " + list.get(i).getMvDistributor());
			}

			// 기준 수치 출력
			System.out.println(" 0          200          400          600          800          1000 ");
			System.out.println("\t\t\t\t\t\t\t  단위:만 명");

			System.out.println("---------------------------------------------------------------------------------");
			System.out.println("1.영화정보 | 2.예매 | 3.마이 페이지 | [4.영화 관리] | [5.회원 관리] | 0.로그아웃");
			System.out.println("---------------------------------------------------------------------------------");
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());
			switch (menu) {
			case 1:
				System.out.println("조회할 영화번호를 입력하세요 :");
				int selectmovie = scanner.nextInt();

				String synopsis = list.get(selectmovie - 1).getMvSynopsis();
				System.out.println(list.get(selectmovie - 1).getMvName());
				System.out.println(list.get(selectmovie - 1).getMvDate());
				System.out.println(list.get(selectmovie - 1).getMvGenre());
				if (synopsis.length() > 70) {
					System.out.print(synopsis.substring(0, 70) + "...\n");
				} else {
					System.out.println(synopsis);
				}
				System.out.println(list.get(selectmovie - 1).getMvDirector());
				System.out.println(list.get(selectmovie - 1).getMvDistributor());
				System.out.println(list.get(selectmovie - 1).getMvRate());
				System.out.println(list.get(selectmovie - 1).getMvAudience());
				System.out.println(list.get(selectmovie - 1).getMvTime());

				// 리뷰 리스트
				System.out.println();
				System.out.println("1. 리뷰보기 | 0. 뒤로가기");

				int selectmovie2 = scanner.nextInt();

				if (selectmovie2 == 0) {
					continue;
				} else if (selectmovie2 == 1) {
					// 리뷰 페이지로 이동
//					view.review(new MemberVO(1, null, null, null, null, null, null, "N", 0, 0), selectmovie);
					
//					System.out.println(list.size()+"////");////
//					for (int i = 0; i < list.size(); i++) {
//						System.out.print(list.get(i).getMvName() + "//");
//					}
					view.review(vo, list.get(selectmovie-1).getMvId());
				} else {
					System.out.println("잘못된 입력입니다");
				}
				break;
				
			case 2:
				ScheduleView scheduleView = new ScheduleView();
				scheduleView.schedule(vo);
				break;
			case 3:
				System.out.println("--------------마이페이지--------------");

				System.out.println("아이디 :" + vo.getMem_id());
				System.out.print("비밀번호 :");
				for (int i = 0; i < vo.getMem_pass().length(); i++) {
					System.out.print("*");

				}
				System.out.println();
				System.out.println("이름 :" + vo.getMem_name());
				System.out.println("전화번호 :" + vo.getMem_hp());
				System.out.println("주소 :" + vo.getMem_addr());
				System.out.println("메일주소 :" + vo.getMem_mail());
				System.out.println("보유 마일리지" + vo.getMem_mileage());
				System.out.println("보유 캐시" + vo.getMem_cash());
				System.out.println("관리자여부" + vo.getMem_flag());

				System.out.println("-----------------------------------");
				System.out.println("1. 캐시충전 || 2.뒤로가기 || 3.구매정보 ||4.비밀번호 변경");
				int select = scanner.nextInt();

				while (true) {
					if (select == 1) {

						System.out.println("충전할 금액을 입력하세요 :");
						int money = scanner.nextInt();
						vo.setMem_cash(vo.getMem_cash() + money);
						int updateMember = memController.updateCash(vo);

						if (updateMember == 1) {
							System.out.println("캐시 충전 완료");
							while (true) {
								System.out.println("1. 뒤로가기");
								int cashBack = scanner.nextInt();

								if (cashBack == 1) {
									break;
								} else {
									System.out.println("잘못된 입력입니다");
									continue;
								}
							}

						} else {
							System.out.println("캐시 충전 실패");
							break;
						}

					} else if (select == 2) {
						break;
					} else if (select == 3) {

						System.out.println("------구매 정보------");

						List<Object> ticketList = BuyController.getTicket(vo.getMem_no());

						for (int i = 0; i < ticketList.size() / 5; i++) {
							System.out.println("영화제목 : " + ((MovieVO) ticketList.get(0 + 5 * i)).getMvName());
							System.out.println("영화 시작 시간 : " + ((ScheduleVO) ticketList.get(1 + 5 * i)).getSch_start());
							System.out.println("좌석 번호 : " + (ticketList.get(2 + 5 * i)));
							System.out.println("티켓 가격 : " + ((BuyVO) ticketList.get(3 + 5 * i)).getBuy_cost());
							System.out.println("구매 일자 : " + ((BuyVO) ticketList.get(4 + 5 * i)).getBuy_date());
							System.out.println(
									"-------------------------------------------------------------------------");
						}
						System.out.println();
						System.out.println();
						System.out.println();
						System.out.println("1. 뒤로가기");
						int back = scanner.nextInt();
						if (back == 1) {
							break;
						} else {
							System.out.println("잘못된 입력입니다");
							break;
						}
					} else if (select == 4) {
						System.out.println("현재 비밀번호를 입력하세요 :");
						String beforePass = scanner.nextLine();
						beforePass = scanner.nextLine();

						if (beforePass.equals(vo.getMem_pass())) {
							System.out.println("변경할 비밀번호를 입력하세요");
							String afterPass = scanner.nextLine();
							if (beforePass.equals(afterPass)) {
								System.out.println("같은 비밀번호로는 변경할수 없습니다.");
							} else {

								vo.setMem_pass(afterPass);
								int updatePass = memController.updatePass(vo);

								if (updatePass == 1) {
									System.out.println("변경완료!");
									System.out.println("1. 뒤로가기");
									int Back = scanner.nextInt();
									if (Back == 1) {
										break;
									} else {
										System.out.println("잘못된 입력입니다");
									}
								} else {
									System.out.println("변경실패!");
								}
							}
						} else {
							System.out.println("비밀번호가 맞지않습니다");
						}

					}else if(select==0) {
						running = false;
						break;
					}

				}
				continue;
			case 4:
				//영화 관리 페이지로 이동.
				MvManageView mvManageView = new MvManageView();
				mvManageView.mvManage();
				
				break;
//			case 5:
//				System.out.println("상영 정보를 입력 해주세요.");
//				break;
			case 5:
				System.out.println("회원 정보");

				List<MemberVO> memlist = memController.getMemberList();
				System.out.printf("\n%-5s %-5s %-7s %-15s %-19s %-1s %-5s %-5s","번호", "아이디","이름","전화번호", "주소", "메일", "관리자" ,"캐시", "마일리지");
				System.out.println();
				for (int i = 0; i < memlist.size(); i++) {

					System.out.print(memlist.get(i).getMem_no() + "\t");
					System.out.print(memlist.get(i).getMem_id() + "\t");
					System.out.print(memlist.get(i).getMem_name() + "\t");
					System.out.print(memlist.get(i).getMem_hp() + "\t");
					System.out.print(memlist.get(i).getMem_addr() + "\t");
					System.out.print(memlist.get(i).getMem_mail() + "\t");
					System.out.print(memlist.get(i).getMem_flag() + "\t");
					System.out.print(memlist.get(i).getMem_cash() + "\t");
					System.out.print(memlist.get(i).getMem_mileage());
					System.out.println();
				}
				System.out.println("1. 회원정보 수정 || 2. 회원정보 삭제 || 3. 뒤로가기");
				int selectmem = scanner.nextInt();
				if (selectmem == 1) {
					System.out.println("수정할 아이디를 입력하세요");
					String updateId = scanner.nextLine();
					updateId = scanner.nextLine();
					MemberVO memvo = memController.getMember(updateId);
					System.out.println("수정할 정보를 선택하세요 1. 전화번호 ||2. 주소 ||3. 메일주소||");
					int selectNum = scanner.nextInt();
					String data = null;
					String data2 = null;
					if (selectNum == 1) {
						data = "mem_hp";
						System.out.println("변경할 전화번호를 입력하세요 :");
						data2 = scanner.nextLine();
						data2 = scanner.nextLine();
					} else if (selectNum == 2) {
						data = "mem_addr";
						System.out.println("변경할 주소를 입력하세요 :");
						data2 = scanner.nextLine();
						data2 = scanner.nextLine();
					} else if (selectNum == 3) {
						data = "mem_mail";
						System.out.println("변경할 메일주소를 입력하세요 :");
						data2 = scanner.nextLine();
						data2 = scanner.nextLine();
					} else {
						System.out.println("잘못된입력입니다");
						break;
					}

					int updateMember = memController.updateMember(data, data2, memvo);
					if (updateMember == 1) {
						System.out.println("업데이트 성공");
						break;
					} else {
						System.out.println("업데이트 실패");
						break;
					}

				} else if (selectmem == 2) {
					System.out.println("삭제할 회원 번호를 입력하세요 :");
					int deleteNum = scanner.nextInt();

					int deleteMem = memController.deleteMember(deleteNum);
					if (deleteMem == 1) {
						System.out.println("삭제 성공");
					} else {
						System.out.println("삭제 실패");
					}
				} else if( selectmem == 3) {
					continue;
				}

			case 0:
				running = false;
				break;
			}
		}
	}

	private void movieMem(MemberVO vo) throws Exception {
		MemberController memController = new MemberController();

		boolean running = true;
		while (running) {
			MovieController controller = MovieController.getInstance();

			List<MovieVO> list = controller.getMovieList(); // 영화 목록 관객수 순으로 가져오도록 수정.

			System.out.println("\n---------------------------------------------------------------------------------");
			System.out.println("\t\t\t현재 상영중인 영화(누적 관객수 순)"); // 등록된 영화 = 상영중인 영화로 변경.
			System.out.println("---------------------------------------------------------------------------------");
//			for (MovieVO movie : list) {
//				System.out.println(movie + "\n");
//			}

			// 관객 수 콤마 찍기
			DecimalFormat formatter = new DecimalFormat("###,###");
//			formatter.format(111);

			// 가로 막대
			for (int i = 0; i < list.size(); i++) {
				for (int j = 0; j < 130; j++) {
					if (j == 0) {
//						System.out.print("   " + date[i] + "   ");
						System.out.print("  ");
					} else {
						System.out.print("■");
						if ((j + 1) * 160000 >= list.get(i).getMvAudience()) {
							System.out.print(" " + (i + 1) + "." + list.get(i).getMvName() + " "
									+ formatter.format(list.get(i).getMvAudience()) + "명");
							break;
						}
					}
				}
//				System.out.println(" " + list.get(i).getMvAudience());
				System.out.println();
//				System.out.println((i+1) + ". " + list.get(i).getMvName() + " | " + list.get(i).getMvGenre() + " | " + list.get(i).getMvRate() + " | " + list.get(i).getMvDirector() + " | " + list.get(i).getMvTime() + " | " + list.get(i).getMvDistributor());
			}

			// 기준 수치 출력
			System.out.println(" 0          200          400          600          800          1000 ");
			System.out.println("\t\t\t\t\t\t\t  단위:만 명");

			System.out.println("------------------------------------------------");
			System.out.println("1.영화정보 | 2.예매 | 3.마이 페이지 | 0.로그아웃");
			System.out.println("------------------------------------------------");

			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);//
			int menu = Integer.parseInt(scanner.nextLine());
			switch (menu) {
			case 1:
				System.out.print("조회할 영화번호를 입력하세요 : ");
				int selectmovie = scanner.nextInt();

				//수정 영화 전체 정보 출력
				System.out.println();
				String synopsis = list.get(selectmovie - 1).getMvSynopsis();
				System.out.println("이름 : " + list.get(selectmovie - 1).getMvName());
				
		        
		        // 포맷터
		        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		 
		        // 문자열 -> Date
		        Date date = dateFormatter.parse(list.get(selectmovie - 1).getMvDate());
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일"); 
				String strNowDate = simpleDateFormat.format(date); 
				System.out.println("개봉일 : " +  strNowDate);
				System.out.println("장르 : " + list.get(selectmovie - 1).getMvGenre());
				if (synopsis.length() > 70) {
					System.out.print("줄거리 : " + synopsis.substring(0, 70) + "...\n");
				} else {
					System.out.println("줄거리 : " + synopsis);
				}
				System.out.println("감독 : " + list.get(selectmovie - 1).getMvDirector());
				System.out.println("배급사 : " + list.get(selectmovie - 1).getMvDistributor());
				System.out.println("등급 : " + list.get(selectmovie - 1).getMvRate());
				System.out.println("누적관객수 : " + formatter.format(list.get(selectmovie - 1).getMvAudience()) + "명");
				System.out.println("상영시간 : " + list.get(selectmovie - 1).getMvTime() + "분");
				System.out.println();

				// 바로 리뷰 페이지로 이동
//				view.review(vo, selectmovie - 1);
				view.review(vo, list.get(selectmovie-1).getMvId());
				
//				int selectmovie2 = scanner.nextInt();
//
//				if (selectmovie2 == 0) {
//					continue;
//					
//					
//				} else if (selectmovie2 == 1) {
//					// 리뷰 페이지로 이동
//				} else {
//					System.out.println("잘못된 입력입니다");
//				}
				break;

			case 2:
				ScheduleView scheduleView = new ScheduleView();
				scheduleView.schedule(vo);
				break;
			case 3:
				System.out.println("---------------------마이페이지---------------------");
				System.out.println("아이디 : " + vo.getMem_id());
				System.out.print("비밀번호 : ");
				for (int i = 0; i < vo.getMem_pass().length(); i++) {
					System.out.print("*");

				}
				System.out.println();
				System.out.println("이름 : " + vo.getMem_name());
				System.out.println("전화번호 : " + vo.getMem_hp());
				System.out.println("주소 : " + vo.getMem_addr());
				System.out.println("메일주소 : " + vo.getMem_mail());
//				System.out.println("보유 마일리지" + vo.getMem_mileage());
				System.out.println("보유 캐시 : " + formatter.format(vo.getMem_cash()) + "원");
//				System.out.println("관리자여부" + vo.getMem_flag());
				System.out.println("----------------------------------------------------");
//				System.out.println("1.캐시 충전 | 2.뒤로가기 | 3.구매 정보 || 4.비밀번호 변경");
				System.out.println("1.캐시 충전 | 2.구매 정보 | 3.비밀번호 변경 | 0.뒤로");
				int select = scanner.nextInt();

				while (true) {
					if (select == 1) {
						System.out.println("충전할 금액을 입력하세요 :");
						int money = scanner.nextInt();
						vo.setMem_cash(vo.getMem_cash() + money);
						int updateMember = memController.updateCash(vo);

						if (updateMember == 1) {
							System.out.println("캐시 충전 완료");
							System.out.println("1. 뒤로가기");
							int cashBack = scanner.nextInt();
							if (cashBack == 1) {
								break;
							} else {
								System.out.println("잘못된 입력입니다");
							}
							break;
						} else {
							System.out.println("캐시 충전 실패");
							break;
						}
					} else if (select == 0) {
						break;
					} else if (select == 2) {
						System.out.println("-------------구매 정보-------------");

						List<Object> ticketList = BuyController.getTicket(vo.getMem_no());

						for (int i = 0; i < ticketList.size() / 5; i++) {
							System.out.println("영화제목 : " + ((MovieVO) ticketList.get(0 + 5 * i)).getMvName());
//							System.out.println("영화 시작 시간 : " + ((ScheduleVO) ticketList.get(1 + 5 * i)).getSch_start());
							System.out.println("영화 시작 시간 : " + ((ScheduleVO) ticketList.get(1 + 5 * i)).getSch_start().substring(0, ((ScheduleVO) ticketList.get(1 + 5 * i)).getSch_start().length() -3));
//							System.out.println("좌석 번호 : <" + ((TicketVO) ticketList.get(2 + 5 * i)).getSt_id() + ">"); //, -> >, <
							System.out.println("좌석 번호 : <" + (ticketList.get(2 + 5 * i)) + ">");

							System.out.println("티켓 가격 : " + formatter.format(((BuyVO) ticketList.get(3 + 5 * i)).getBuy_cost()) + "원");
							System.out.println("구매 일자 : " + ((BuyVO) ticketList.get(4 + 5 * i)).getBuy_date());
							System.out.println("-----------------------------------");
						}
						System.out.println();
						System.out.println("1. 뒤로가기");
						int back = scanner.nextInt();
						if (back == 1) {
							break;
						} else {
							System.out.println("잘못된 입력입니다");
							break;
						}
					} else if (select == 3) {
						System.out.print("현재 비밀번호를 입력하세요 :");
						scanner = new Scanner(System.in);
						String beforePass = scanner.nextLine();

						if (beforePass.equals(vo.getMem_pass())) {
							System.out.println("변경할 비밀번호를 입력하세요");
							String afterPass = scanner.nextLine();
							if (beforePass.equals(afterPass)) {
								System.out.println("같은 비밀번로는 변경할수 없습니다.");
							} else {
								vo.setMem_pass(afterPass);
								int updatePass = memController.updatePass(vo);

								if (updatePass == 1) {
									System.out.println("변경완료!");
									System.out.println("1. 뒤로가기");
									int Back = scanner.nextInt();
									if (Back == 1) {
										break;
									} else {
										System.out.println("잘못된 입력입니다");
									}
								} else {
									System.out.println("변경실패!");
									break;
								}
							}
						}
					} else {
						System.out.println("비밀번호가 맞지않습니다");
						break;
					}
				}

				continue;

			case 0:
				running = false;
				break;
			}
		}
	}
}
