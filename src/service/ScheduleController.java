package service;

import java.util.List;

import vo.MovieVO;
import vo.ScheduleVO;

public class ScheduleController {
	private static ScheduleController instance = new ScheduleController();

	private ScheduleController() {

	}

	public static ScheduleController ScheduleController() {
		return instance;
	}

	ScheduleService service = ScheduleService.getInstance();

	public List<ScheduleVO> getScheduleList() throws Exception {
		return service.getScheduleList();
	}

	public static ScheduleController getInstance() {
		return instance;
	}

	public ScheduleVO getSchedule(String searchSch_start) throws Exception {
		return service.getSchedule(searchSch_start);
	}

}
