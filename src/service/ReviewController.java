package service;

import java.util.List;

import vo.ReviewVO;
import vo.TicketVO;

public class ReviewController {
	ReviewService service = ReviewService.getInstance();

	public List<ReviewVO> getRevListByMvId(int mvId) throws Exception {
		return service.getRevListByMvId(mvId);//
	}

	public ReviewVO getReview(int searchNo) throws Exception {
		return service.getReview(searchNo);
	}

	public int insertReview(ReviewVO vo) throws Exception {
		return service.insertReview(vo);
	}

	public int updateReview(ReviewVO vo) throws Exception {
		return service.updateReview(vo);
	}

	public int deleteReview(int deleteReview) throws Exception {
		return service.deleteReview(deleteReview);
	}

	public TicketVO getTicListByMemNo(int memNo) throws Exception {
		return service.getTicListByMemNo(memNo);
	}

}
