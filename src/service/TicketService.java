package service;

import java.util.ArrayList;

import dao.TicketDAO;
import vo.TicketVO;

public class TicketService {
	TicketDAO dao = TicketDAO.getInstance();

	private static TicketService instance = new TicketService();

	private TicketService() {

	}

	public static TicketService getInstance() {
		return instance;
	}

	public ArrayList<TicketVO> getTicListBySch_no(int timeMenu) throws Exception {
		return dao.getTicListBySch_no(timeMenu);
	}

	public int insertTicket(TicketVO ticVO) throws Exception {
		return dao.insertTicket(ticVO);
	}

}
