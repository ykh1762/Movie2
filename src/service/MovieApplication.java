package service;
//commit 전에 DAO - ip 변경하기(C+A+g)

//@192.168.34.47
//@192.168.34.47
//@localhost
//@localhost
//MovieDAO, ScheduleDAO, TicketDAO

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MovieApplication {
	public static void main(String[] args) throws Exception {
		MainView view = new MainView();
		view.init();
	}
}

//View 메뉴 선택 틀
//boolean running = true;
//MovieView view = new MovieView();
//while (running) { 
//System.out.println("------------------------------------");
//System.out.println("(1.회원가입)|2.로그인|3.종료|(4.임시 로그인)");
//System.out.println("------------------------------------");
//	System.out.print("선택> ");
//	Scanner scanner = new Scanner(System.in);
//	int menu = Integer.parseInt(scanner.nextLine());
//	switch (menu) {
//	case 1:
//		break;
//	case 2:
//		break;
//	case 3:
//		running = false;
//		break;
//	case 4:
//		view.movie();
//		break;
//	}
//}		

//Dao (DAO?) 메서드 틀
//DriverManager.registerDriver(new OracleDriver());
//Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@192.168.34.47:1521:xe", "project_team4", "java");
//PreparedStatement statement = null;
//
//StringBuilder builder = new StringBuilder();
//builder.append("");
//
//String sql = builder.toString();
//statement = connection.prepareStatement(sql);
//statement.setInt(1, vo.getMv_no);
//statement.setString(2, vo.getMv_name);
//
//ResultSet resultSet = statement.executeQuery();
//List<ScheduleVO> list = new ArrayList<>();
//while (resultSet.next()) {
//	int sch_no = resultSet.getInt("sch_no");
//	String sch_start = resultSet.getString("sch_start");
//	list.add(new ScheduleVO(sch_no, sch_start, mv_id));
//}
//resultSet.close();
//
////int insertInt = statement.executeUpdate(sql);
//statement.close();
//connection.close();
//
//return list;

//singleton 틀
//private static SeatController instance = new SeatController();
//private SeatController() {
//	
//}
//public static SeatController getInstance() {
//	return instance;
//}
