package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import vo.MemberVO;
import vo.MovieVO;
import vo.ScheduleVO;
import vo.Seat_productVO;
import vo.TicketVO;

public class SeatView {
//	SeatController controller = SeatController.getInstance();
	TicketController ticController = TicketController.getInstance();
	ScheduleController schController = ScheduleController.getInstance();
	MovieController mvController = MovieController.getInstance();
	private boolean cancelFlag = false;
	private List<Integer> seatList;

	public void seat(String strMenu, MemberVO memVO) throws Exception {
		BuyView buyView = new BuyView();
		boolean running = true;
		while (running) {
//			running = false;
			String[] strings = strMenu.split("/");
			String chooseDay = strings[0];
			String chooseTime = strings[1];
			int chooseNumber = Integer.parseInt(strings[2]);

			String searchSch_start = "2022/11/" + chooseDay + " " + chooseTime + ":00";

			// getSchedule(searchSch_no) "2022-11-" + checkDay + " " + checkTime + ":00"
			ScheduleVO schVO = schController.getSchedule(searchSch_start);

			// 영화번호로 영화 조회.
			MovieVO mvVO = mvController.getMvByMvId(schVO.getMv_id());

			// getTicketList. 상영 번호로 티켓 목록을 조회.
			ArrayList<TicketVO> list = ticController.getTicListBySch_no(schVO.getSch_no());
			seatList(list);

			//// 상영시간 컬럼을 가져와서 13:00 ~ (13:00+상영시간+10분)
			//// 남은 좌석 34/50 -> 어디에 표시?
			//// 요일 추가 - date로 요일 가져오고 - 2022/11/19 (토)
			System.out.println("------------------------------------");
			System.out.println(mvVO.getMvName() + " | " + searchSch_start + " | " + chooseNumber + "명");
			System.out.println("좌석을 선택해 주세요. (0/" + chooseNumber + ") | 0.취소");
			System.out.println("------------------------------------");

			chooseSeat(list, mvVO, searchSch_start, chooseNumber);

			if (cancelFlag) {
				break;
			}

			// 선택 완료된 상태
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());
			switch (menu) {
			case 1:
				// 자리 선택 완료 -> 결제 페이지
				System.out.println("결제 페이지로");////
				// MovieVO mvVO, ScheduleVO schVO, List<Seat_productVO> SPList, MemberVO memVO
				buyView.buy(mvVO, schVO, seatList, memVO);
				running = false; //
				break;
			case 0:
//				running = false;
				break;
			default:
				System.out.println("메뉴 번호를 확인해주세요.");
				continue;
			}
		}
	}

	private void chooseSeat(List<TicketVO> list, MovieVO mvVO, String searchSch_start, int chooseNumber) {
		String totalSeatStr = "선택 좌석 : "; // 선택한 좌석 표시
		int checkNumber = 0; // 선택한 좌석 수
		seatList = new ArrayList<>();

		// 여러 좌석 예매하는 경우 선택 좌석 표시 해주고 모두 선택되면 flag 변경.
		boolean seatFlag = false;

		while (!seatFlag) {
			List<Integer> buyStList = new ArrayList<>();

//			boolean buyFlag = false;
			for (int i = 1; i < 51; i++) {
				for (int j = 0; j < list.size(); j++) {
					if (i == list.get(j).getSt_id()) {
//						System.out.println(list.get(j).getSt_id());//
//						buyFlag = true;
						buyStList.add(i);
					}
				}
			}
//			System.out.println(buyFlag);//

			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());

			if (menu == 0) {
				cancelFlag = true;
				break;
			} else if (menu < 0 || menu > 50) {
				System.out.println("좌석 번호를 확인 해주세요.");
				continue;
			}

			boolean buyFlag = false;

			for (int i = 0; i < buyStList.size(); i++) {
				if (menu == buyStList.get(i)) {
					buyFlag = true;
				}
			}

			//// 선택한 좌석을 좌석 목록에 표시.
//			 C [21] [22] <23> [24] [25]    [26] [27] [28] [29] [30]   
//			 D [31] [32] [33] <34> [35]    [36] [37] [38] [39] [■■]   
//			 E [41] [42] [■■] [44] [45]    [46] [47] [48] [49] [50]  
			//// 선택한 좌석 선택하면 - 이미 선택한 좌석입니다.

			boolean chooseFlag = false; // 이미 선택한 좌석인지 여부.
			for (int i = 0; i < seatList.size(); i++) {
				if (seatList.get(i) == menu) {
					chooseFlag = true;
				}
			}

			if (buyFlag) {
				System.out.println(menu + "번 좌석은 예매할 수 없습니다. 다시 선택해주세요.");
				continue;
			} else if (chooseFlag) {
				System.out.println("이미 선택한 좌석입니다.");
				continue;
			} else {
				seatList.add(menu);
				checkNumber++;

				String modifiedMenu = "";
				if (menu < 10) {
					modifiedMenu = "0" + menu;
				} else {
					modifiedMenu = String.valueOf(menu);
				}

				if (checkNumber == 1) {
					totalSeatStr += "<" + modifiedMenu + ">";
				} else {
					totalSeatStr += ", <" + modifiedMenu + ">";
				}
			}

			String numberStr = " (" + checkNumber + "/" + chooseNumber + ")";

			seatList(list);

			if (checkNumber == chooseNumber) {
				System.out.println("------------------------------------");
				System.out.println(mvVO.getMvName() + " | " + searchSch_start + " | " + chooseNumber + "명");
				System.out.println(totalSeatStr + numberStr + " 좌석 선택 완료!");
				System.out.println("------------------------------------");
				System.out.println("1.결정 | 0.취소");
				System.out.println("------------------------------------");

				seatFlag = true;
			} else {
				System.out.println("------------------------------------");
				System.out.println(mvVO.getMvName() + " | " + searchSch_start + " | " + chooseNumber + "명");
				System.out.println(totalSeatStr + numberStr);
				System.out.println("------------------------------------");
				System.out.println("좌석을 선택해 주세요. | 0.취소");
				System.out.println("------------------------------------");
			}
		}
	}

	private void seatList(List<TicketVO> list) {
		System.out.println("\n\t\t     S  C  R  E  E  N  \n");
		// 01~50에서 A1~E10으로 변경. 행은 좌석 맨 왼쪽에 한 번만 표시. 'A'==65
		// 한 좌석 출력할 때 for로 목록 조회해서 st_id와 좌석 번호가 일치하면 ■■
		for (int i = 1; i < 51; i++) {
			boolean buyFlag = false;
			for (int j = 0; j < list.size(); j++) {
				if (i == list.get(j).getSt_id()) {
					buyFlag = true;
				}
			}
			char rowStr = 'A';
			switch (i / 10) {
			case 0:
				break;
			case 1:
				rowStr = (char) ((int) rowStr + (i / 10));
				break;
			case 2:
				rowStr = (char) ((int) rowStr + (i / 10));
				break;
			case 3:
				rowStr = (char) ((int) rowStr + (i / 10));
				break;
			case 4:
				rowStr = (char) ((int) rowStr + (i / 10));
				break;
			}

			if (i % 10 == 1) {
				if (buyFlag) {
					if (i % 5 == 0) {
						if ((i) % 10 == 0) {
							System.out.print(" " + rowStr + " [■■]   \n");
						} else {
							System.out.print(" " + rowStr + " [■■]    ");
						}
					} else {
						if ((i) % 10 == 0) {
							System.out.print(" " + rowStr + " [■■]\n");
						} else {
							System.out.print(" " + rowStr + " [■■] ");
						}
					}
				} else {
					if (i % 5 == 0) {
						if ((i) % 10 == 0) {
							if (i < 10) {
//							String modifiedI = "0" + i;
								String modifiedI = String.valueOf("0" + (i));
								System.out.print(" " + rowStr + " [" + modifiedI + "]   \n");
							} else {
								System.out.print(" " + rowStr + " [" + (i) + "]   \n");
							}
						} else {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print(" " + rowStr + " [" + modifiedI + "]    ");
							} else {
								System.out.print(" " + rowStr + " [" + (i) + "]    ");
							}
						}
					} else {
						if ((i) % 10 == 0) {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print(" " + rowStr + " [" + modifiedI + "]\n");
							} else {
								System.out.print(" " + rowStr + " [" + (i) + "]\n");
							}
						} else {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print(" " + rowStr + " [" + modifiedI + "] ");
							} else {
								System.out.print(" " + rowStr + " [" + (i) + "] ");
							}
						}
					}
				}
			} else {
				if (buyFlag) {
					if (i % 5 == 0) {
						if ((i) % 10 == 0) {
							System.out.print("[■■]   \n");
						} else {
							System.out.print("[■■]    ");
						}
					} else {
						if ((i) % 10 == 0) {
							System.out.print("[■■]\n");
						} else {
							System.out.print("[■■] ");
						}
					}
				} else {
					if (i % 5 == 0) {
						if ((i) % 10 == 0) {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print("[" + modifiedI + "]   \n");
							} else {
								System.out.print("[" + (i) + "]   \n");
							}
						} else {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print("[" + modifiedI + "]    ");
							} else {
								System.out.print("[" + (i) + "]    ");
							}
						}
					} else {
						if ((i) % 10 == 0) {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print("[" + modifiedI + "]\n");
							} else {
								System.out.print("[" + (i) + "]\n");
							}
						} else {
							if (i < 10) {
								String modifiedI = String.valueOf("0" + (i));
								System.out.print("[" + modifiedI + "] ");
							} else {
								System.out.print("[" + (i) + "] ");
							}
						}
					}
				}
			}
		}
		System.out.println("\n\t\t\t\t  [■■] : 예매 불가 좌석");
	}
}
