package service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import vo.MemberVO;
import vo.MovieVO;

//영화 관리 페이지
public class MvManageView {
	public void mvManage() throws Exception {
		boolean running = true;
//		MovieView view = new MovieView();
		while (running) { 
			MovieController controller = MovieController.getInstance();
			
			System.out.println("\n-------------------------------------------------");
			System.out.println("1.영화 등록 | 2.영화 수정 | 3. 영화 삭제 | 0.뒤로");
			System.out.println("-------------------------------------------------");
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
			int menu = Integer.parseInt(scanner.nextLine());
			switch (menu) {
			case 1:
				System.out.println("영화 정보를 입력 해주세요.");
				System.out.print("제목>");
				String mv_name = scanner.nextLine();
				System.out.print("개봉일>");
				String mv_date = scanner.nextLine();
				System.out.print("장르>");
				String mv_genre = scanner.nextLine();
				System.out.print("줄거리>");
				String mv_synopsis = scanner.nextLine();
				System.out.print("감독>");
				String mv_director = scanner.nextLine();
				System.out.print("배급사>");
				String mv_distributor = scanner.nextLine();
				System.out.print("등급>");
				String mv_rate = scanner.nextLine();
				System.out.print("누적관객수>");
				int mv_audience = scanner.nextInt();
				System.out.print("상영시간>");
				int mv_time = scanner.nextInt();
				int insertMemo = controller.insertMovie(new MovieVO(mv_audience, mv_name, mv_date, mv_genre,
						mv_synopsis, mv_director, mv_distributor, mv_rate, mv_audience, mv_time));
				if (insertMemo == 1) {
					System.out.println("영화 등록 완료!");
				} else {
					System.out.println("영화 등록 실패!");
				}
				break;
			case 2:
				//영화 목록 출력
				List<MovieVO> mvList = controller.getMovieList();
				
				System.out.println();
				int i = 0;
				DecimalFormat formatter = new DecimalFormat("###,###");
				for (MovieVO movieVO : mvList) {
					System.out.println((i+1) + ". " + movieVO.getMvName() + " | " + formatter.format(movieVO.getMvAudience()) + "명 | " + movieVO.getMvGenre() + " | " + movieVO.getMvRate() + " | " + movieVO.getMvDirector() + " | " + movieVO.getMvTime() + "분 | " + movieVO.getMvDistributor());
//					formatter.format(111);
					i++;
				}
				
				System.out.println("\n------------------------------------");
				System.out.print("수정할 영화 번호를 입력하세요. > ");
				scanner = new Scanner(System.in);
				menu = Integer.parseInt(scanner.nextLine());
				MovieVO selectMv = controller.getMvByMvId(mvList.get(menu-1).getMvId());
				
//				String updateId = scanner.nextLine();
//				MemberVO memvo = memController.getMember(updateId);
				
				//수정 영화 전체 정보 출력
				System.out.println();
				String synopsis = selectMv.getMvSynopsis();
				System.out.println("1.이름 : " + selectMv.getMvName());
				
		        
		        // 포맷터
		        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		 
		        // 문자열 -> Date
		        Date date = dateFormatter.parse(selectMv.getMvDate());
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일"); 
				String strNowDate = simpleDateFormat.format(date); 
				System.out.println("2.개봉일 : " +  strNowDate);
				System.out.println("3.장르 : " + selectMv.getMvGenre());
				if (synopsis.length() > 70) {
					System.out.print("4.줄거리 : " + synopsis.substring(0, 70) + "...\n");
				} else {
					System.out.println("4.줄거리 : " + synopsis);
				}
				System.out.println("5.감독 : " + selectMv.getMvDirector());
				System.out.println("6.배급사 : " + selectMv.getMvDistributor());
				System.out.println("7.등급 : " + selectMv.getMvRate());
				System.out.println("8.누적관객수 : " + formatter.format(selectMv.getMvAudience()) + "명");
				System.out.println("9.상영시간 : " + selectMv.getMvTime() + "분");
				System.out.println();
				
				System.out.println("1.이름|2.개봉일|3.장르|4.줄거리|5.감독|6.배급사|7.등급|8.누적관객수|9.상영시간");
				System.out.print("수정할 정보를 선택하세요. > "); ////(memdao)
				int selectNum = scanner.nextInt();
				String data = null;
				String data2 = null;
				if (selectNum == 1) {
					data = "mv_name";
					System.out.println("변경할 이름을 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 2) {
					data = "mv_date";
					System.out.println("변경할 개봉일을 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 3) {
					data = "mv_genre";
					System.out.println("변경할 장르를 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 4) {
					data = "mv_synopsis";
					System.out.println("변경할 줄거리를 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 5) {
					data = "mv_director";
					System.out.println("변경할 감독을 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 6) {
					data = "mv_distributor";
					System.out.println("변경할 배급사를 입력하세요 :");
					data2 = scanner.nextLine();
				} else if (selectNum == 7) {
					data = "mv_rate";
					System.out.println("변경할 등급을 입력하세요. > ");
					data2 = scanner.nextLine();
				} else if (selectNum == 8) {
					data = "mv_audience";
					System.out.println("변경할 누적관객수를 입력하세요. > ");
					data2 = scanner.nextLine();
					data2 = scanner.nextLine();
				} else if (selectNum == 9) {
					data = "mv_time";
					System.out.println("변경할 상영시간을 입력하세요. > ");
					data2 = scanner.nextLine();
				} else {
					System.out.println("잘못된입력입니다");
					break;
				}

				int updateMovie = controller.updateMovie(data, data2, selectMv);
				if (updateMovie == 1) {
					System.out.println("업데이트 성공");
					break;
				} else {
					System.out.println("업데이트 실패");
					break;
				}
			case 3:
//				view.movie();
				break;
			case 0:
				running = false;
				break;
			}
		}		
	}
}
