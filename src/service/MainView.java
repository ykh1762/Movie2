package service;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import vo.MemberVO;

public class MainView {//
	// View 흐름
	// MainView
	// 1.(회원 가입)
	// 2.로그인
	// 3.종료
	// 4.(임시 로그인) - MovieView
		// 2.예매 - ScheduleView
			// 1~5.'날짜 선택' & '시간 선택' - SeatView
			// 6.뒤로
	// 예매 완료 후 MainView로 이동.
	MemberController controller = new MemberController();
//
	Scanner scn = new Scanner(System.in);

	public void init() throws Exception {
		boolean running = true;
		MovieView view = new MovieView();

		while (running) {

			System.out.println("\n"
					+ "⣿⣿⣿⣿⣿⣿⣿⠟⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠿⣿⣿⣿⣿⣿⣿⣿\n"
					+ "⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿\n"
					+ "⣿⣿⣿⠟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣦⢠⣄⢀⣤⣤⣤⣀⣠⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠻⣿⣿⣿\n"
					+ "⣿⡟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⡀⢀⣿⣿⣿⣿⣿⣦⣿⣿⣿⣿⣿⣯⣠⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢻⣿\n"
					+ "⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⢠⣤⡤⠶⠶⣦⡀⠀⠀⠀⠀⠀⢻⣿⣿⠉⠉⣿⣿⣟⠛⠛⠛⣿⣿⡏⠉⢹⣿⣿⡇⠀⠀⠀⠀⣠⣶⣤⣤⣦⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣷⡀⠀⢀⠀⠀⢸⣿⣿⠀⠀⣿⣿⣧⣤⣤⣤⣿⣿⡇⠀⢸⣿⣿⡇⠀⠀⣠⣾⣿⣿⣿⣿⣿⣿⣦⡄⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣛⡴⣋⣴⣇⢸⣿⣿⡀⢀⣿⡿⢛⠉⡉⢙⡻⣿⣇⠀⢸⣿⣿⠃⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣛⣯⣵⣾⣿⣿⣿⣾⣿⣿⡇⣼⣿⠁⡯⠿⠭⡯⠽⢸⣿⡆⢸⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣯⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠿⠟⠒⠋⠉⠉⠉⠙⠚⠻⠧⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⣿⣿⣿⣿⣿⠿⠟⠉⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⡖⠦⢤⡉⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢿⣿⡟⠋⢠⣤⣤⣤⡀⠀⠀⠀⠀⠀⣰⣿⣿⡃⠀⣿⣿⡀⠀⠙⢻⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢈⠟⢁⣶⣿⣿⣿⣧⣾⡆⠀⠀⠀⠀⢻⣿⣿⣿⣶⣿⣿⠃⠀⠀⠀⢻⣿⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠎⠀⠸⣿⣿⣿⣿⣿⣿⡟⠀⢀⡀⠀⠀⠛⠿⣿⣿⠿⠋⠀⠀⠀⠀⠀⢹⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡸⠀⠀⠀⠙⢿⣿⣿⣿⠟⠁⠀⠘⢛⣀⣀⠿⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀⠀⠀⠰⠿⢠⣾⣿⣿⣿⣿⣆⢠⣦⠀⠀⢀⣀⣀⠀⠀⠀⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⣀⣠⣀⡀⠀⢀⡀⣿⣿⣿⣿⣿⣿⣿⠀⠁⢠⣾⣿⣿⠀⠈⢢⠀⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⢠⠊⠀⣿⣿⣿⣦⠸⠟⠘⢿⣿⣿⣿⣿⠏⢶⡦⣾⣿⣿⣿⠀⠀⠀⠆⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢰⠀⡇⠀⠀⠸⣿⣿⣿⠇⠀⢠⣶⠌⠉⢩⣤⠀⠀⠀⠘⢿⣿⠇⠀⢀⠜⠀⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢂⠘⢄⡀⠀⠘⣿⠟⠀⠀⠀⠀⣀⣤⣬⣅⡀⠀⠀⠀⠀⠉⠉⠉⠁⠀⡜⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢢⡀⠈⠉⠉⠀⠀⠀⠀⢀⣾⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⢀⣜⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\n"
					+ "⣦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠑⢄⠀⠀⠀⠀⠀⠀⠸⠉⠙⠛⠋⠉⠁⡸⠀⠀⠀⠀⠀⣠⣾⣿⣿⣷⣶⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣴\n"
					+ "⣿⣷⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠑⠢⣀⠀⠀⠀⠀⠱⢄⡀⠀⣀⠴⠁⠀⠀⣀⠔⠋⠀⠙⢿⡟⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣾⣿\n"
					+ "⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠒⠢⠤⠤⠀⣈⠉⠤⠤⠔⠒⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿\n"
					+ "⣿⣿⣿⣿⣿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣿⣿⣿⣿⣿\n"
					+ "⣿⣿⣿⣿⠿⠿⠿⠷⠤⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠤⠾⠿⠿⠿⣿⣿⣿⣿\n"
					+ "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠰⠴⠶⠦⠶⠶⠆⠆⠶⠶⠶⠠⠀⠇⠅⠇⠅⠇⠆⠆⠆⠀\n"
					+ "");

			
			// View 표시(개발 완료 후 제거)

			System.out.println("----------------------------------------------------------------");
			System.out.println("\t\t 영화, 그 이상의 감동 STARBOX");
			System.out.println("----------------------------------------------------------------");
			System.out.println("                1.회원 가입 | 2.로그인 | 0.종료");
			System.out.println("----------------------------------------------------------------");
			System.out.print("선택> ");
			Scanner scanner = new Scanner(System.in);
//				int menu = Integer.parseInt(scanner.nextLine());
			int menu = 0;
			String temp = scanner.nextLine();

			// 숫자인지 확인
			if (isNumeric(temp)) {
				menu = Integer.parseInt(temp);
			} else {
				System.out.println("잘못된입력입니다.");
				continue;
			}

			switch (menu) {
			case 1:
				String id;
				while (true) {
					System.out.print("회원가입할 아이디를 입력하세요> ");
					id = scn.nextLine();
					Pattern check = Pattern.compile("^[a-zA-Z0-9]*$");
					boolean find = check.matches("^[a-zA-Z0-9]*$", id);

					if (find == true) {
						System.out.println("사용가능한 아이디입니다. ");
						break;
					} else {
						System.out.println("부적절한 아이디입니다. ");
						continue;
					}
				}

				String pass;
				while (true) {
					System.out.print("비밀번호를 설정해주세요> ");
					pass = scn.nextLine();
					Pattern check = Pattern.compile("^[a-zA-Z0-9]*$");
					boolean find = check.matches("^[a-zA-Z0-9]*$", pass);
					if (find == true) {

						break;
					} else {
						System.out.println("올바르지 않은 비밀번호입니다. ");
						continue;
					}
				}
				System.out.println("이름을 입력하세요 : ");
				String name = scn.nextLine();

				System.out.println("전화번호를 입력하세요 : ");
				String hp = scn.nextLine();
				System.out.println("주소를 입력하세요 : ");
				String addr = scn.nextLine();
				System.out.println("메일주소를 입력하세요 :");
				String mail = scn.nextLine();
				if (controller.getMember(id) == null) {
					int insertMember = controller.insertMember(new MemberVO(id, pass, name, hp, addr, mail));

					if (insertMember == 1) {
						System.out.println("회원 등록 완료!");
					} else {
						System.out.println("회원 등록 실패!");
					}
				} else {
					System.out.println("이미 존재하는 ID입니다.");
				}

				break;
			case 2:
				while (true) {

					System.out.print("아이디를 입력하세요 : ");
					String loginId = scn.nextLine();
					Pattern check = Pattern.compile("^[a-zA-Z0-9]*$");
					boolean find = check.matches("^[a-zA-Z0-9]*$", loginId);

					if (find == true) {
//						System.out.println("사용가능한 아이디입니다. ");

					} else {
						System.out.println("부적절한 아이디입니다.");
						continue;
					}
					System.out.print("비밀번호를 입력하세요 : ");
					String loginPass = scn.nextLine();
					Pattern check2 = Pattern.compile("^[a-zA-Z0-9]*$");
					boolean find2 = check.matches("^[a-zA-Z0-9]*$", loginPass);

					if (find2 == true) {
//						System.out.println("사용가능한 비밀번호입니다. ");

					} else {
						System.out.println("부적절한 비밀번호입니다.");
						continue;
					}

					MemberVO vo = null;
					vo = controller.getMember(loginId);

					if (vo == null) {
						System.out.println("ID가 틀렸습니다.");
					} else {
						if (loginId.equals(vo.getMem_id())) {
							if (loginPass.equals(vo.getMem_pass())) {
								System.out.println("로그인 성공! ");
								view.movie(vo);
								break;

							} else {
								System.out.println("비밀번호가틀렸습니다.");
							}
						} else {
							System.out.println("ID가 틀렸습니다.");
						}
					}
				}
			case 3:
				break;
			case 4:
//				view.movie(new MemberVO(1, null, null, null, null, null, null, "N", 0, 0));
//				break;
				continue;
			default:
				System.out.println("잘못된입력입니다.");
				continue;

			case 0:
				running = false;
				break;
			}
		}

	}

	public static boolean isNumeric(String s) {
		try {
			Double.parseDouble(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
