package service;

import java.util.List;

import dao.ScheduleDAO;
import vo.MovieVO;
import vo.ScheduleVO;

public class ScheduleService {
	private static ScheduleService instance = new ScheduleService();

	private ScheduleService() {

	}

	public static ScheduleService getInstance() {
		return instance;
	}

	ScheduleDAO dao = ScheduleDAO.getInstance();

	public List<ScheduleVO> getScheduleList() throws Exception {
		return dao.getScheduleList();
	}

	public ScheduleVO getSchedule(String searchSch_start) throws Exception {
		return dao.getSchedule(searchSch_start);
	}

}
